package com.GalaxyIO;

import java.time.LocalDate;

public class Cliente {

    private int id;
    private String nombre;
    private String numero;
    private String cedula;
    private String rif;
    private LocalDate cumpleanos;

    public Cliente(int id, String nombre, String numero, String cedula, String rif, LocalDate cumpleanos) {
        this.id = id;
        this.nombre = nombre;
        this.numero = numero;
        this.cedula = cedula;
        this.rif = rif;
        this.cumpleanos = cumpleanos;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getNombre() {
        return nombre;
    }

    public void setNombre(String nombre) {
        this.nombre = nombre;
    }

    public String getNumero() {
        return numero;
    }

    public void setNumero(String numero) {
        this.numero = numero;
    }

    public String getCedula() {
        return cedula;
    }

    public void setCedula(String cedula) {
        this.cedula = cedula;
    }

    public String getRif() {
        return rif;
    }

    public void setRif(String rif) {
        this.rif = rif;
    }

    public LocalDate getCumpleanos() {
        return cumpleanos;
    }
}
