package com.GalaxyIO;

import com.GalaxyIO.views.AdminPresenter;

import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;
import java.util.Objects;

public class Costumer {

    private int userID;
    private String tecnico;
    private String name;
    private String number;
    private String dd;
    private int tiempoDeServ;
    private String servicio;
    private LocalDateTime date;
    private int confirmed;
    private String evento;
    private String pago;
    private String nota;
    private int index;


    public Costumer(int userID, String tecnico, String name, String number, String servicio, int tiempoDeServ, LocalDateTime date, int confirmed, String evento, String pago, String nota, int index) {

        DateTimeFormatter formatters = DateTimeFormatter.ofPattern("EEEE , dd/MMMM/yyyy");

        DateTimeFormatter formatter = DateTimeFormatter.ofPattern("hh:mm a");

        this.tecnico = tecnico;
        this.userID = userID;
        this.name = name;
        this.number = number;
        this.servicio = servicio;
        this.tiempoDeServ = tiempoDeServ;
        this.date = date;
        this.dd = date.format(formatters) + ", " + date.format(formatter);
        this.confirmed = confirmed;
        this.index = index;
        this.evento = evento;
        this.pago = pago;
        this.nota = nota;

    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getNumber() {

        if (number.length() < 11) {

            return "00000000000";

        } else {

            return number;
        }
    }

    public void setNumber(String number) {
        this.number = number;
    }

    public LocalDateTime getDate() {
        return date;
    }

    public void setDate(LocalDateTime date) {
        this.date = date;
    }

    public String getDd() {
        return dd;
    }

    public void setDd(String dd) {
        this.dd = dd;
    }

    public int getTiempoDeServ() {
        return tiempoDeServ;
    }

    public void setTiempoDeServ(int tiempoDeServ) {
        this.tiempoDeServ = tiempoDeServ;
    }

    public String getServicio() {
        return servicio;
    }

    public void setServicio(String servicio) {
        this.servicio = servicio;
    }

    public String getTecnico() {

        String[] name = tecnico.split("\\.");

        return name[0];
    }

    public void setTecnico(String tecnico) {
        this.tecnico = tecnico;
    }

    public String getUserID() {

        if (AdminPresenter.userArrayList != null) {

            for (int i = 0; i < AdminPresenter.userArrayList.size(); i++) {

                if (userID == AdminPresenter.userArrayList.get(i).getUserID()) {

                    return (userID + " (" + AdminPresenter.userArrayList.get(i).getUserName() + ")");

                }

            }
        }

        return String.valueOf(userID);
    }

    public void setUserID(int userID) {
        this.userID = userID;
    }

    public int getConfirmed() {
        return confirmed;
    }

    public void setConfirmed(int confirmed) {
        this.confirmed = confirmed;
    }

    public String getEvento() {
        return evento;
    }

    public void setEvento(String evento) {
        this.evento = evento;
    }

    public String getPago() {
        return pago;
    }

    public void setPago(String pago) {
        this.pago = pago;
    }

    public String getNota() {
        return nota;
    }

    public void setNota(String nota) {
        this.nota = nota;
    }

    public int getIndex() {
        return index;
    }

    public void setIndex(int index) {
        this.index = index;
    }

    @Override
    public String toString() {
        return
                        "Nombre: '" + name + '\'' +
                        ", Telefono: '" + number + '\'' +
                        ", Tecnico: '" + tecnico + '\'' +
                        ", Fecha: " + date;
    }

    @Override
    public boolean equals(Object o) {

        if (o == this) return true;
        if (!(o instanceof Costumer)) {
            return false;
        }
        Costumer costumer = (Costumer) o;
        return Objects.equals(name, costumer.name) &&
                Objects.equals(number, costumer.number) &&
                Objects.equals(tecnico, costumer.tecnico) &&
                Objects.equals(date, costumer.date);

    }

    @Override
    public int hashCode() {
        return Objects.hash(name, number, date, tecnico);
    }

}

