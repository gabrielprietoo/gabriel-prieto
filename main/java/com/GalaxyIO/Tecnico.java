package com.GalaxyIO;

public class Tecnico {

    private String nombre;
    private String diaLibre;
    private int gridPlacementX;
    private int gridPlacementY;


    public Tecnico(String nombre, String diasLibre) {
        this.nombre = nombre;
        this.diaLibre = diasLibre;
    }

    public String getDiaLibre() {

        String day;

        switch (diaLibre) {
            case "L":
                day = "MONDAY";
                break;
            case "M":
                day = "TUESDAY";
                break;
            case "MM":
                day = "WEDNESDAY";
                break;
            case "J":
                day = "THURSDAY";
                break;
            case "V":
                day = "FRIDAY";
                break;
            case "S":
                day = "SATURDAY";
                break;
            default:

                day = "NOTLIBRE";
                break;
        }

        return day;
    }

    public void setDiaLibre(String diaLibre) {
        this.diaLibre = diaLibre;
    }

    public String getNombre() {
        return nombre;
    }

    public void setNombre(String nombre) {
        this.nombre = nombre;
    }

    public int getGridPlacementX() {
        return gridPlacementX;
    }

    public void setGridPlacementX(int gridPlacementX) {
        this.gridPlacementX = gridPlacementX;
    }

    public int getGridPlacementY() {
        return gridPlacementY;
    }

    public void setGridPlacementY(int gridPlacementY) {
        this.gridPlacementY = gridPlacementY;
    }

    @Override
    public String toString() {

        String day;

        if (diaLibre.equals("L")) {
            day = "Lunes";
        } else if (diaLibre.equals("M")) {
            day = "Martes";
        } else {
            day = "Miercoles";
        }

        return "Tecnico{" +
                "nombre='" + nombre + '\'' +
                ", diaLibre='" + day + '\'' +
                '}';
    }
}
