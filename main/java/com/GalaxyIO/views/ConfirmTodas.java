package com.GalaxyIO.views;

import com.GalaxyIO.Connectivity;
import com.GalaxyIO.Costumer;
import com.GalaxyIO.SohoApp;
import com.gluonhq.charm.glisten.control.Dialog;
import com.gluonhq.charm.glisten.control.ProgressBar;
import com.google.common.collect.HashMultiset;
import com.google.common.collect.Multiset;
import javafx.application.Platform;
import javafx.concurrent.Task;
import javafx.fxml.FXML;
import javafx.scene.control.Label;
import javafx.scene.input.MouseEvent;
import javafx.scene.layout.AnchorPane;
import net.swisstech.bitly.BitlyClient;
import net.swisstech.bitly.model.Response;
import net.swisstech.bitly.model.v3.ShortenResponse;

import java.time.LocalDate;
import java.time.format.DateTimeFormatter;
import java.util.ArrayList;

public class ConfirmTodas {

    public static Dialog dialog;
    @FXML
    private AnchorPane menu;
    @FXML
    private AnchorPane loader;
    @FXML
    private ProgressBar progressBar;
    @FXML
    private Label totalCitas;
    @FXML
    private Label confirmedCitas;

    @FXML
    void secondMessage(MouseEvent event) {
        if (AdminPresenter.currentPageDate.equals(LocalDate.now().plusDays(1))) {

            menu.setVisible(false);
            loader.setVisible(true);

            ArrayList<Costumer> toConfirm = new ArrayList<>();

            for (Costumer costumer : SohoApp.list) {

                if (costumer.getConfirmed() == 1 && costumer.getDate().toLocalDate().equals(AdminPresenter.currentPageDate)) {

                    toConfirm.add(costumer);

                }

            }

            totalCitas.setText("Total: " + toConfirm.size() + " citas.");

            confirmedCitas.setText(0 + " citas confirmadas");

            Task update = new Task() {
                @Override
                protected Object call() throws Exception {
                    final int[] index = {1};

                    for (Costumer costumer : toConfirm) {

                        int cConfirmed = costumer.getConfirmed();

                        if (costumer.getConfirmed() == 1 && costumer.getDate().toLocalDate().equals(AdminPresenter.currentPageDate)) {

                            BitlyClient client = new BitlyClient("58567062bcb8e4b9115f92015a22c45efd0d5687");

                            Response<ShortenResponse> respShort = client.shorten() //
                                    .setLongUrl("http://sohonails.com.ve/test.php?index=" + costumer.getIndex())
                                    .call();

                            String msg = costumer.getName() + ", no hemos recibido tu confirmación, Porfavor haga click en este link: " + respShort.data.url
                                    + " y confirma tu cita! Att. Soho Nail Spa";

                            System.out.println(msg);

                            Platform.runLater(() -> {

                                confirmedCitas.setText(index[0] + " citas confirmadas");

                                double progress = (double) index[0] / toConfirm.size();

                                progressBar.setProgress(progress);

                                index[0]++;

                            });

                            if (Connectivity.sendSMS(costumer.getNumber(), costumer.getName(), msg)) {


                                System.out.println(msg);

                            }
                        }
                    }

                    return null;
                }
            };

            new Thread(update).start();
        }

    }


    @FXML
    void firstMessage(MouseEvent event) {

        if (AdminPresenter.currentPageDate.equals(LocalDate.now().plusDays(1))) {
            menu.setVisible(false);
            loader.setVisible(true);

            ArrayList<Costumer> toConfirm = new ArrayList<>();

            Multiset<Costumer> wordsMultiset = HashMultiset.create();
            wordsMultiset.addAll(SohoApp.list);

            Task update = new Task() {
                @Override
                protected Object call() throws Exception {

                    final int[] index = {1};

                    for (Multiset.Entry<Costumer> entry : wordsMultiset.entrySet()) {

                        if (entry.getElement().getConfirmed() == 0 && entry.getElement().getDate().toLocalDate().equals(AdminPresenter.currentPageDate)) {

                            BitlyClient client = new BitlyClient("58567062bcb8e4b9115f92015a22c45efd0d5687");

                            Response<ShortenResponse> respShort = client.shorten() //
                                    .setLongUrl("http://sohonails.com.ve/test.php?index=" + entry.getElement().getIndex())
                                    .call();
                            String msg;

                            if (entry.getCount() > 1) {

                                msg = entry.getElement().getName() +
                                        ", tienes " + entry.getCount() + " citas para el " + entry.getElement().getDate().toLocalDate().format(DateTimeFormatter.ofPattern("EEEE, dd 'de' MMMM yyyy"))
                                        + ". Para confirmar haga click en este link: " + respShort.data.url;

                            } else {

                                msg = entry.getElement().getName() +
                                        ", tu cita ha sido pautada para el " + entry.getElement().getDate().toLocalDate().format(DateTimeFormatter.ofPattern("EEEE, dd 'de' MMMM yyyy"))
                                        + ", " + entry.getElement().getDate().toLocalTime().format(DateTimeFormatter.ofPattern("hh:mm a"))
                                        + ". Para confirmar haga click en este link: " + respShort.data.url;

                            }

                            if (Connectivity.sendSMS(entry.getElement().getNumber(), entry.getElement().getName(), msg)) {

                                SohoApp.connectivity.setConfirmed(entry.getElement().getIndex());

                                Platform.runLater(() -> {

                                    confirmedCitas.setText(index[0] + " citas confirmadas");

                                    double progress = (double) index[0] / wordsMultiset.entrySet().size();

                                    progressBar.setProgress(progress);

                                    index[0] += 1;

                                });

                                System.out.println(msg);

                            }

                        }

                    }

                    return null;
                }
            };

            update.setOnFailed(event1 -> {

                update.getException().printStackTrace();
                new Thread(update).start();

            });

            update.setOnSucceeded(event1 -> {
                dialog.hide();
            });

            new Thread(update).start();

        }
    }
}