package com.GalaxyIO.views;

import com.GalaxyIO.Cliente;
import com.GalaxyIO.SohoApp;
import com.gluonhq.charm.glisten.control.DatePicker;
import com.gluonhq.charm.glisten.control.Dialog;
import com.gluonhq.charm.glisten.control.Icon;
import com.jfoenix.controls.JFXProgressBar;
import javafx.beans.value.ChangeListener;
import javafx.beans.value.ObservableValue;
import javafx.css.PseudoClass;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.scene.control.TextField;
import javafx.scene.input.KeyCode;
import javafx.scene.input.MouseEvent;
import org.apache.commons.lang3.text.WordUtils;

import java.io.IOException;
import java.sql.SQLException;
import java.time.LocalDate;
import java.time.format.DateTimeFormatter;
import java.util.concurrent.atomic.AtomicBoolean;
import java.util.concurrent.atomic.AtomicReference;

public class AddToList {

    @FXML
    private Icon callIcon;

    @FXML
    private TextField numero;

    @FXML
    private Icon nombreIcon;

    @FXML
    private TextField cliente;

    @FXML
    private TextField cumpleAnos;

    @FXML
    private Icon tiempIcon;

    @FXML
    private TextField tiempoServicio;

    @FXML
    private Icon fechIcon;

    @FXML
    private TextField fecha;

    @FXML
    private Icon tecIcon;

    @FXML
    private TextField tecnico;

    @FXML
    public JFXProgressBar progressBar;

    @FXML
    void initialize(){

        System.out.println("Dialog Presenter Initiated");

        progressBar.setVisible(false);

        AtomicBoolean isCliente = new AtomicBoolean();

        AtomicReference<KeyCode> keyCode = new AtomicReference<KeyCode>();

        keyCode.set(KeyCode.ENTER);

        cliente.textProperty().addListener(new ChangeListener<String>() {
            @Override
            public void changed(ObservableValue<? extends String> observable, String oldValue, String newValue) {

                cliente.setText(WordUtils.capitalizeFully(newValue));


            }
        });

        cumpleAnos.setOnKeyPressed(event -> {

            keyCode.set(event.getCode());
            System.out.println(event.getCode());

        });

        cumpleAnos.textProperty().addListener(new ChangeListener<String>() {
            @Override
            public void changed(ObservableValue<? extends String> observable, String oldValue, String newValue) {

                if (keyCode.get() != KeyCode.BACK_SPACE) {

                    if (newValue.length() > 10) {

                        cumpleAnos.setText(oldValue);

                    } else if (newValue.length() == 2 || newValue.length() == 5) {

                        cumpleAnos.setText(newValue + "/");

                    }

                }

            }
        });

        numero.textProperty().addListener(new ChangeListener<String>() {
            @Override
            public void changed(ObservableValue<? extends String> observable, String oldValue,
                                String newValue) {

                //cliente.setDisable(true);

                isCliente.set(false);

                if (!newValue.matches("\\d{0,11}([\\.]\\d{0,4})?")) {
                    System.out.println("not a number: " + oldValue);

                    numero.setText(oldValue);

                } else if (newValue.length() == 11) {

                    System.out.println("cell complete");

                    for (Cliente clienteLoop : SohoApp.clienteList) {

                        if (clienteLoop.getNumero().equals(newValue)) {

                            System.out.println("found cell match");
                            cliente.setText(clienteLoop.getNombre());
                            cumpleAnos.setText(String.valueOf(clienteLoop.getCumpleanos()));
                            //cliente.setDisable(true);
                            //isCliente.set(true);

                            break;
                        }

                    }

                    if (!isCliente.get()) {

                        //cliente.setDisable(false);

                    }
                }

            }
        });

    }

    public boolean checkError() {

        if (!numero.getText().isEmpty() && numero.getText().length() == 11 && !cliente.getText().isEmpty() && TecnicoPicker.date != null) {
            try {

                return true;

            } catch (Exception e) {

                e.printStackTrace();

            }

        } else if (tiempoServicio.getText().isEmpty()) {

            tiempoServicio.pseudoClassStateChanged(PseudoClass.getPseudoClass("error"), true);

            return false;

        } else if (TecnicoPicker.date == null) {

            fecha.pseudoClassStateChanged(PseudoClass.getPseudoClass("error"), true);

            return false;

        } else if (numero.getText().length() != 11) {

            numero.pseudoClassStateChanged(PseudoClass.getPseudoClass("error"), true);

            return false;

        } else if (cliente.getText().isEmpty()) {

            cliente.pseudoClassStateChanged(PseudoClass.getPseudoClass("error"), true);

            return false;

        } else {

            tecnico.pseudoClassStateChanged(PseudoClass.getPseudoClass("error"), true);
            numero.pseudoClassStateChanged(PseudoClass.getPseudoClass("error"), true);
            cliente.pseudoClassStateChanged(PseudoClass.getPseudoClass("error"), true);
            fecha.pseudoClassStateChanged(PseudoClass.getPseudoClass("error"), true);
            tiempoServicio.pseudoClassStateChanged(PseudoClass.getPseudoClass("error"), true);

            return false;

        }

        return false;

    }

    public void agregar() throws IOException, SQLException {

        if (checkError()) {

            String nombre = cliente.getText();

            String telefono = numero.getText();

            LocalDate date = TecnicoPicker.date;

            String tecnicoS;

            String servicio;

            if (tecnico.getText().isEmpty()) {

                tecnicoS = " ";

            } else {

                tecnicoS = tecnico.getText();

            }

            if (tiempoServicio.getText().isEmpty()) {

                servicio = " ";

            } else {

                servicio = tiempoServicio.getText();

            }

            SohoApp.connectivity.setLista(nombre, telefono, date, tecnicoS, servicio);

        }

    }

    @FXML
    private void pickDate(MouseEvent event) {

        fechIcon.requestFocus();
        DatePicker datePicker = new DatePicker();
        datePicker.showAndWait().ifPresent(System.out::println);

        LocalDate localDate = datePicker.getDate();

        if (localDate.getDayOfWeek().name().equals("SUNDAY")) {

            TecnicoPicker.date = localDate.plusDays(1);

        } else {

            TecnicoPicker.date = localDate;

        }

        //TecnicoPicker.tiempoServ = tiemp;

        TecnicoPicker.date = localDate;

        fecha.setText(TecnicoPicker.date.format(DateTimeFormatter.ofPattern("EEEE , dd MMMM yyyy")));


    }

    @FXML
    void pickTecnico(MouseEvent event) {

        fechIcon.requestFocus();

        Dialog dialog = new Dialog();

        FXMLLoader loader;

        try {

            loader = new FXMLLoader(
                    getClass().getResource(
                            "tecnicopicker.fxml"
                    )
            );


            dialog.setContent(loader.load());

            TecnicoPicker.dialog = dialog;

            dialog.showAndWait();

            String name = TecnicoPicker.selected.getText();

            tecnico.setText(name);


            dialog.hide();

        } catch (IOException e) {
            e.printStackTrace();
        }

    }

    @FXML
    void pickProduct(MouseEvent event) {

        fechIcon.requestFocus();

        Dialog dialog = new Dialog();

        FXMLLoader loader;

        try {

            loader = new FXMLLoader(
                    getClass().getResource(
                            "productPicker.fxml"
                    )
            );


            dialog.setContent(loader.load());

            ProductPicker.dialog = dialog;

            dialog.showAndWait();

            String name = ProductPicker.selected.getText();

            tiempoServicio.setText(name);

            dialog.hide();

        } catch (IOException e) {
            e.printStackTrace();
        }
    }
}
