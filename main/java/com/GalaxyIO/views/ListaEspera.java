package com.GalaxyIO.views;

import com.GalaxyIO.Costumer;
import com.GalaxyIO.SohoApp;
import com.gluonhq.charm.glisten.control.Dialog;
import com.gluonhq.charm.glisten.control.Icon;
import com.jfoenix.controls.JFXProgressBar;
import javafx.application.Platform;
import javafx.concurrent.Task;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.scene.control.Button;
import javafx.scene.control.TextField;
import javafx.scene.layout.AnchorPane;
import javafx.scene.layout.VBox;

import java.io.IOException;
import java.sql.SQLException;
import java.util.ArrayList;

public class ListaEspera {

    public static Dialog dialog;
    ArrayList<Costumer> lista = new ArrayList<>();
    @FXML
    private VBox vBox;
    @FXML
    private Icon callIcon;
    @FXML
    private TextField numero;
    @FXML
    private Icon nombreIcon;
    @FXML
    private TextField cliente;
    @FXML
    private TextField cumpleAnos;
    @FXML
    private Icon tiempIcon;
    @FXML
    private TextField tiempoServicio;
    @FXML
    private Icon fechIcon;
    @FXML
    private TextField fecha;
    @FXML
    private Icon tecIcon;
    @FXML
    private TextField tecnico;
    @FXML
    private JFXProgressBar progressBar;

    @FXML
    void initialize() {

        int index = 0;

        try {
            lista = SohoApp.connectivity.getListaEspera();
        } catch (SQLException e) {
            e.printStackTrace();
        }


        for (int j = 0; j < lista.size(); j++) {

            if (lista.get(j).getDate().toLocalDate().equals(AdminPresenter.currentPageDate)) {

                Button button = new Button((j + 1) + ". " + lista.get(j).getName() + ", " + lista.get(j).getNumber() + ", " + lista.get(j).getTecnico() + ", " + lista.get(j).getServicio());

                button.setOnSwipeLeft(event -> {

                    System.out.println("SWIPED");
                    vBox.getChildren().clear();
                    initialize();

                });

                button.setOnMouseClicked(event -> {


                });

                AnchorPane anchorPane = new AnchorPane();

                anchorPane.getChildren().add(button);

                AnchorPane.setRightAnchor(button, 0.0);
                AnchorPane.setLeftAnchor(button, 0.0);
                AnchorPane.setTopAnchor(button, 0.0);
                AnchorPane.setBottomAnchor(button, 0.0);


                vBox.getChildren().add(anchorPane);

                index++;

            }

        }


    }

    @FXML
    void agregar() throws IOException {


        Dialog dialog = new Dialog();

        try {

            FXMLLoader loader = new FXMLLoader(
                    getClass().getResource(
                            "addToList.fxml"
                    )
            );

            dialog.setContent(loader.load());
            Button okButton = new Button("Agregar");
            okButton.setOnAction(e -> {

                AddToList controller = loader.getController();

                if (controller.checkError()) {

                    Task<String> task = new Task<String>() {

                        @Override
                        protected String call() {
                            try {
                                controller.agregar();

                            } catch (NullPointerException e) {

                                //System.out.print(e);
                                return "Interrupted!";
                            } catch (SQLException e1) {
                                e1.printStackTrace();
                            } catch (IOException e1) {
                                e1.printStackTrace();
                            }

                            return "Done!";
                        }
                    };

                    task.setOnFailed(ex -> {
                        task.getException().printStackTrace();
                        // inform user of error...
                    });

                    task.setOnSucceeded(ex -> {

                        controller.progressBar.setVisible(false);

                        dialog.hide();

                        vBox.getChildren().clear();

                        initialize();

                    });

                    new Thread(task).start();

                    controller.progressBar.progressProperty().bind(task.progressProperty());
                    controller.progressBar.setVisible(true);

                } else {

                    Platform.runLater(() -> {

                        Dialog dialog1 = new Dialog("Numero de telefono ocupado!", "Esta segura de hacer una cita para el mismo dia?");

                        Button si = new Button("SI");
                        Button no = new Button("NO");

                        Task<String> duplicada = new Task<String>() {

                            @Override
                            protected String call() {
                                try {
                                    controller.agregar();

                                } catch (NullPointerException e) {

                                    //System.out.print(e);
                                    return "Interrupted!";
                                } catch (SQLException e1) {
                                    e1.printStackTrace();
                                } catch (IOException e1) {
                                    e1.printStackTrace();
                                }

                                return "Done!";
                            }
                        };

                        duplicada.setOnFailed(ex -> {
                            duplicada.getException().printStackTrace();
                            // inform user of error...
                        });

                        duplicada.setOnSucceeded(ex -> {

                            controller.progressBar.setVisible(false);

                            dialog.hide();

                        });

                        si.setOnMouseClicked(event -> {

                            dialog1.hide();
                            new Thread(duplicada).start();
                            controller.progressBar.progressProperty().bind(duplicada.progressProperty());
                            controller.progressBar.setVisible(true);


                        });

                        no.setOnMouseClicked(event -> dialog1.hide());

                        dialog1.getButtons().add(si);

                        dialog1.getButtons().add(no);

                        dialog1.showAndWait();

                        System.out.println("CELL DECTEDTED");

                    });

                }

            });

            dialog.getButtons().add(okButton);
            dialog.showAndWait();

        } catch (IOException e) {
            e.printStackTrace();
        }

    }


}
