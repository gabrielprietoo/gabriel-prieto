package com.GalaxyIO.views;

import com.gluonhq.charm.glisten.mvc.View;
import javafx.fxml.FXMLLoader;

import java.io.IOException;

public class PrimaryView {

    private final String name;


    public PrimaryView() {

        this.name = "";
    }

    public PrimaryView(String name) {
        this.name = name;
    }

    public View getView() {
        try {
            View view = FXMLLoader.load(PrimaryView.class.getResource("primary.fxml"));
            return view;
        } catch (IOException e) {
            System.out.println("IOException: " + e);
            return new View();
        }
    }
}
