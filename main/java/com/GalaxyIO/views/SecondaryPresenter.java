package com.GalaxyIO.views;

import com.GalaxyIO.SohoApp;
import com.gluonhq.charm.glisten.control.Dialog;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.scene.input.MouseEvent;
import javafx.scene.layout.HBox;
import javafx.scene.layout.Priority;

import java.io.IOException;
import java.sql.SQLException;
import java.util.Locale;

public class SecondaryPresenter extends AdminPresenter {

    @Override
    @FXML
    public void initialize() {
        Locale.setDefault(new Locale("es", "ES"));

        super.initialize();
        this.createCitasTable();
        //startCalendar();
        //refresher();
    }

    @FXML
    void calendarTable(MouseEvent event) {


    }

    @Override
    public void createCitasTable() {

        String[] user = PrimaryPresenter.userName.split("\\.");

        try {
            arraylist = SohoApp.connectivity.getCustomersfor(user[0]);
        } catch (SQLException e) {
            e.printStackTrace();
        }

        CalendarArraylist = SohoApp.list;

        secondary.setShowTransitionFactory(com.gluonhq.charm.glisten.animation.BounceInUpTransition::new);

        secondary.showingProperty().addListener((obs, oldValue, newValue) -> {

            HBox.setHgrow(agregarCita, Priority.ALWAYS);
            HBox.setHgrow(dispBtn, Priority.ALWAYS);
            HBox.setHgrow(listaDeEspera, Priority.ALWAYS);
            HBox.setHgrow(calendarButton, Priority.ALWAYS);

            agregarCita.setPrefWidth(100000);
            dispBtn.setPrefWidth(100000);
            listaDeEspera.setPrefWidth(100000);
            calendarButton.setPrefWidth(100000);

            populate();

        });


    }

    @Override
    void populate() {

        data = getInitialTableData(arraylist);

    }


    @Override
    public void makeDialog(int index, String tecnico) {

        Dialog dialog = new Dialog();

        FXMLLoader loader = new FXMLLoader(getClass().getResource(
                "calendarDialog.fxml"
        ));

        try {
            dialog.setContent(loader.load());
        } catch (IOException e) {
            e.printStackTrace();
        }

        dialog.showAndWait();

    }
}