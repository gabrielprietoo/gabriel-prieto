package com.GalaxyIO.views;

import javafx.fxml.FXML;
import javafx.scene.Node;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.scene.control.TextArea;
import javafx.scene.layout.AnchorPane;
import javafx.scene.layout.VBox;

import java.time.LocalDate;
import java.time.format.DateTimeFormatter;

public class calendarDialog {

    public static int sCumple;
    static String sNombre;
    static String sNumero;
    static String sServicio;
    static int sTiempo;
    static String sFecha;
    static String sTecnico;
    static String sEvento;
    static String sPago;
    static String sNota;
    static int sIndex;
    static int sConfirmada;

    @FXML
    private AnchorPane anchor;

    @FXML
    private Label appBar;
    @FXML
    private Label nombre;
    @FXML
    private Label numero;
    @FXML
    private Label servicio;
    @FXML
    private Label tiempo;
    @FXML
    private Label fecha;
    @FXML
    private Label tecnico;

    @FXML
    private Label evento;

    @FXML
    private Label pago;

    @FXML
    private Label nota;

    @FXML
    private VBox vBox;


    @FXML
    void initialize() {

        nombre.setText("Nombre: " + sNombre);
        numero.setText("Numero: " + sNumero);
        servicio.setText("Servicio: " + sServicio);
        tiempo.setText("Tiempo: " + sTiempo);
        fecha.setText("Fecha: " + String.valueOf(LocalDate.parse(sFecha, DateTimeFormatter.ofPattern("yyyy-MM-dd HH:mm")).format(DateTimeFormatter.ofPattern("EEEE , dd MMMM yyyy"))));
        tecnico.setText("Tecnico: " + sTecnico);
        evento.setText("Evento: " + sEvento);
        pago.setText("Pago: " + sPago);
        nota.setText("Nota: " + sNota);

        for (Node label : vBox.getChildren()) {

            if(label.getClass().getName().equals("javafx.scene.control.Label")){

                label.setOnMouseClicked(event -> {

                    com.gluonhq.charm.glisten.control.Dialog dialog = new com.gluonhq.charm.glisten.control.Dialog();

                    TextArea textArea = new TextArea();

                    textArea.setText(((Label)label).getText());

                    textArea.setEditable(false);

                    Button close = new Button("Cerrar");

                    close.setOnMouseClicked(event1 -> dialog.hide());

                    dialog.getButtons().add(close);

                    dialog.setContent(textArea);

                    dialog.showAndWait();

                });

            }

        }


    }

    static void clearAll(){

        sNombre = null;
        sNumero = null;
        sServicio = null;
        sTiempo = 0;
        sFecha = null;
        sTecnico = null;
        sEvento = null;
        sPago = null;
        sNota = null;
        sIndex = 0;
        sConfirmada = 0;

    }

    public AnchorPane getAnchor() {
        return anchor;
    }

    public void setAnchor(AnchorPane anchor) {
        this.anchor = anchor;
    }
}