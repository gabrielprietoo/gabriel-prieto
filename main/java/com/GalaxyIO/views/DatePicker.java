package com.GalaxyIO.views;

import com.gluonhq.charm.glisten.control.BottomNavigationButton;
import com.gluonhq.charm.glisten.control.Dialog;
import com.gluonhq.charm.glisten.visual.Swatch;
import com.gluonhq.charm.glisten.visual.SwatchElement;
import javafx.fxml.FXML;
import javafx.geometry.HPos;
import javafx.geometry.VPos;
import javafx.scene.Node;
import javafx.scene.input.MouseEvent;
import javafx.scene.layout.GridPane;
import javafx.scene.layout.StackPane;
import javafx.scene.paint.Paint;
import javafx.scene.shape.Circle;
import javafx.scene.text.Text;

import java.time.LocalDate;
import java.time.Month;
import java.time.YearMonth;
import java.time.format.TextStyle;
import java.util.ArrayList;
import java.util.Locale;
import java.util.concurrent.atomic.AtomicReference;

public class DatePicker {

    static int realDay, realMonth, realYear, currentMonth, currentYear;
    static ArrayList<LocalDate> dates = new ArrayList<>();

    static Dialog dialog;

    static ArrayList<Node> nodes = new ArrayList<>();

    @FXML
    private GridPane gridPane;

    @FXML
    private BottomNavigationButton previous;

    @FXML
    private BottomNavigationButton Date;

    @FXML
    private BottomNavigationButton next;

    @FXML
    void initialize() {

        dates.clear();

        Locale.setDefault(new Locale("es", "ES"));

        realDay = LocalDate.now().getDayOfMonth(); //Get day
        realMonth = LocalDate.now().getMonthValue(); //Get month
        realYear = LocalDate.now().getYear(); //Get year
        currentMonth = realMonth; //Match month and year
        currentYear = realYear;

        Date.setText(Month.of(currentMonth).getDisplayName(TextStyle.FULL, new Locale("es", "ES")).toUpperCase());

        createCalendar(currentMonth, currentYear);

    }

    void createCalendar(int currentMonth, int currentYear) {

        Date.requestFocus();

        int nod, som; //Number Of Days, Start Of Month

        nod = YearMonth.of(currentYear, currentMonth).lengthOfMonth(); //number of days

        som = LocalDate.of(currentYear, currentMonth, 1).getDayOfWeek().getValue(); //first day of month

        //System.out.println(som);

        previous.setDisable(false);
        next.setDisable(false);

        System.out.println(currentMonth);

        if (currentMonth == 1) {
            previous.setDisable(true);
        } //Too early

        if (currentMonth == 12) {
            next.setDisable(true);
        } //Too late

        //Draw calendar

        for (int i = 1; i <= nod; i++) {

            int row = ((i + som - 2) / 7);

            int column = (i + som - 2) % 7;

            StackPane stackPane = new StackPane();

            Text cDay = new Text(String.valueOf(i));

            stackPane.getChildren().add(cDay);

                AtomicReference<Boolean> selected = new AtomicReference<>(false);

                LocalDate localDate = LocalDate.of(currentYear, currentMonth, i);

                stackPane.setOnMouseClicked(event -> {

                    if (dates.size() < 4 && !selected.get()) {
                        selected.set(true);
                        stackPane.getChildren().add(0, new Circle(0, 0, 20, Paint.valueOf(Swatch.getDefault().getColor(SwatchElement.PRIMARY_100).toString())));

                        dates.add(localDate);

                    } else if (selected.get() && dates.size() <= 4) {
                        selected.set(false);
                        stackPane.getChildren().remove(0);
                        dates.remove(localDate);

                    }

                });

            GridPane.setHalignment(stackPane, HPos.CENTER);
            GridPane.setValignment(stackPane, VPos.CENTER);

            gridPane.add(stackPane, column, row + 1);

            nodes.add(stackPane);

        }

    }

    @FXML
    void next(MouseEvent event) {

        Date.fire();
        currentMonth += 1;
        gridPane.getChildren().removeAll(nodes);
        Date.setText(Month.of(currentMonth).getDisplayName(TextStyle.FULL, new Locale("es", "ES")).toUpperCase());
        createCalendar(currentMonth, currentYear);
        Date.fire();

    }

    @FXML
    void previous(MouseEvent event) {

        Date.fire();
        currentMonth -= 1;
        gridPane.getChildren().removeAll(nodes);
        Date.setText(Month.of(currentMonth).getDisplayName(TextStyle.FULL, new Locale("es", "ES")).toUpperCase());
        createCalendar(currentMonth, currentYear);
        Date.fire();
    }

    @FXML
    void confirmar(MouseEvent event) {

        System.out.println(dates);

        dialog.hide();

    }

    ArrayList<LocalDate> getDates() {

        return dates;

    }

    @FXML
    void eliminar(MouseEvent event) {

        dates.clear();

        gridPane.getChildren().removeAll(nodes);

        createCalendar(currentMonth, currentYear);
    }


}