package com.GalaxyIO.views;

import com.GalaxyIO.Producto;
import com.GalaxyIO.SohoApp;
import com.gluonhq.charm.glisten.control.Dialog;
import com.gluonhq.charm.glisten.control.Icon;
import com.gluonhq.charm.glisten.visual.MaterialDesignIcon;
import javafx.fxml.FXML;
import javafx.geometry.Pos;
import javafx.scene.CacheHint;
import javafx.scene.control.Button;
import javafx.scene.control.ContentDisplay;
import javafx.scene.control.Label;
import javafx.scene.input.MouseEvent;
import javafx.scene.layout.GridPane;

import java.util.ArrayList;
import java.util.concurrent.atomic.AtomicBoolean;

public class ProductPicker {

    public static Button selected = null;

    public static Dialog dialog;

    public static int tiempo;

    @FXML
    private GridPane gridPane;

    @FXML
    private Label selectedLabel;

    @FXML
    private Button simultaneo;

    @FXML
    void initialize() {

        selected = null;

        selectedLabel.setText("Selecione uno o dos servicios");

        ArrayList<Button> bList = new ArrayList<>();

        ArrayList<Button> selectedButtons = new ArrayList<>();

        for (Producto producto : SohoApp.productoList) {

            Button button = new Button(producto.getNombreProducto());

            button.setPrefWidth(500);
            button.setPrefHeight(500);

            button.setCache(true);
            button.setCacheHint(CacheHint.SPEED);

            button.alignmentProperty().setValue(Pos.CENTER);

            AtomicBoolean twoHour = new AtomicBoolean();
            twoHour.set(false);

            button.setOnMouseClicked(event -> {

                 if (selectedButtons.size() < 2 && !twoHour.get()) {

                    if (selectedButtons.size() == 0) {

                        selectedLabel.setText(button.getText());
                        tiempo = getTiempo(button.getText());
                        TecnicoPicker.tiempoServ = tiempo;
                        System.out.println(tiempo);

                    } else {

                        tiempo += getTiempo(button.getText());
                        selectedLabel.setText(selectedLabel.getText() + " + " + button.getText());
                        System.out.println(tiempo);
                        TecnicoPicker.tiempoServ = tiempo;

                    }

                    selectedButtons.add(button);
                    button.setDisable(true);
                    button.setGraphic(new Icon(MaterialDesignIcon.CHECK_BOX));
                    button.setContentDisplay(ContentDisplay.RIGHT);

                } else {
                    selectedButtons.get(0).setDisable(false);
                    selectedButtons.get(0).setGraphic(null);
                    selectedButtons.get(1).setGraphic(null);
                    selectedButtons.get(1).setDisable(false);
                    selectedButtons.clear();
                    selectedLabel.setText("Selecione uno o dos servicios");
                    tiempo = 0;


                }

            });

            bList.add(button);
        }
        int index = 0;

        for (int i = 0; i < 2; i++) {

            for (int j = 0; j < 5; j++) {

                if (index < SohoApp.productoList.size()) {

                    gridPane.add(bList.get(index), i, j);
                    index++;
                }
            }

        }//JFXScrollPane.smoothScrolling(sp);
        gridPane.setVgap(10);
    }

    @FXML
    void simul(MouseEvent event) {

        if(!TecnicoPicker.simultaneo){

            TecnicoPicker.simultaneo = true;
            simultaneo.setGraphic(new Icon(MaterialDesignIcon.CHECK_BOX));

        }else{

            TecnicoPicker.simultaneo = false;
            simultaneo.setGraphic(null);
        }


    }

    @FXML
    void confirm(MouseEvent event) {

        if(tiempo <= 2){

            selected = new Button(selectedLabel.getText());
            dialog.hide();

        }

    }

    int getTiempo(String name) {

        for (Producto product : SohoApp.productoList) {

            if (product.getNombreProducto().equals(name)) {

                return product.getTiempoProducto();

            }

        }

        return 0;
    }

}
