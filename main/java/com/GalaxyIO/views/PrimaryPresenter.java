package com.GalaxyIO.views;

import com.GalaxyIO.Connectivity;
import com.GalaxyIO.SohoApp;
import com.gluonhq.charm.glisten.application.MobileApplication;
import com.gluonhq.charm.glisten.control.AppBar;
import com.gluonhq.charm.glisten.control.Dialog;
import com.gluonhq.charm.glisten.control.TextField;
import com.gluonhq.charm.glisten.mvc.View;
import com.jfoenix.controls.JFXProgressBar;
import javafx.concurrent.Task;
import javafx.fxml.FXML;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.scene.control.PasswordField;
import javafx.scene.input.KeyCode;
import javafx.scene.input.MouseEvent;
import javafx.scene.layout.AnchorPane;

import java.math.BigInteger;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import java.security.SecureRandom;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.Arrays;

public class PrimaryPresenter {

    public static int userID = 0;
    public static String userName;
    //public static Multimap<Cliente, String> citasMap = HashMultimap.create();
    private String passw = null;
    private String salt = null;
    private String hassh = null;
    private String saltedHash = null;
    private String role = null;
    @FXML
    private View primary;

    @FXML
    private Button ingresar;

    @FXML
    private TextField user;

    @FXML
    private PasswordField pass;

    @FXML
    private JFXProgressBar progress;

    @FXML
    private AnchorPane loginContainer;


    public void initialize() {

        getDB();
        ingresar.setDisable(true);
        primary.showingProperty().addListener((obs, oldValue, newValue) -> {
            if (newValue) {
                AppBar appBar = MobileApplication.getInstance().getAppBar();
                appBar.setTitleText("Soho");
            }
        });

        pass.setOnKeyPressed(event -> {

            if (event.getCode() == KeyCode.ENTER && !ingresar.isDisable()) {

                logIn();
                ingresar.setDisable(true);

            }
        });

        user.setOnKeyPressed(event -> {

            if (event.getCode() == KeyCode.ENTER) {

                pass.requestFocus();

            }
        });


    }


    @FXML
    void btnIngresar(MouseEvent event) {


        logIn();
        ingresar.setDisable(true);


    }

    void logIn() {

        primary.requestFocus();

        Task<ArrayList<String>> widgetSearchTask = new Task<ArrayList<String>>() {
            @Override

            public ArrayList<String> call() throws Exception {

                return SohoApp.connectivity.getValue(user.getText(), pass.getText(), "userId", "salt", "role");

            }
        };

        widgetSearchTask.setOnFailed(e -> {
            widgetSearchTask.getException().printStackTrace();
            // inform user of error...
        });

        widgetSearchTask.setOnSucceeded(e -> {
                    // Task.getValue() gives the value returned from call()...
                    ArrayList<String> hello = widgetSearchTask.getValue();

                    if (hello.size() != 0) {
                        userID = Integer.parseInt(hello.get(1));
                        salt = hello.get(2);
                        passw = pass.getText();
                        saltedHash = hello.get(0);
                        role = hello.get(3);
                        hassh = hash(salt + passw);

                        helo();

                    }else{

                        Dialog dialog = new Dialog();
                        dialog.setTitle(new Label("Usuario no encontrado!"));

                        Button okButton = new Button("OK");
                        okButton.setOnAction(event -> {
                            ingresar.setDisable(false);
                            dialog.hide();
                        });
                        dialog.getButtons().add(okButton);
                        dialog.showAndWait();
                    }
                    System.out.println("Task done!");
                    progress.setVisible(false);
                }
        );


        // run the task using a thread from the thread pool:

        progress.progressProperty().bind(widgetSearchTask.progressProperty());
        progress.setVisible(true);

        new Thread(widgetSearchTask).start();
    }

    void helo() {

        try {

            if (hassh.equals(saltedHash) && (role.equals("admin") || role.equals("secretaria"))) {

                System.out.println("ADMIN AUTHENTICATED!");
                userName = user.getText();
                System.out.println("User: " + userName + " " + "With ID: " + userID);
                ingresar.setDisable(false);
                MobileApplication.getInstance().switchView("Admin View");

            } else if (hassh.equals(saltedHash)) {


                System.out.println("AUTHENTICATED!");
                userName = user.getText();
                System.out.println("User: " + userName + " " + "With ID: " + userID);
                ingresar.setDisable(false);
                MobileApplication.getInstance().switchView("Secondary View");

            } else {

                Dialog dialog = new Dialog();
                dialog.setTitle(new Label("Usuario no encontrado!"));

                Button okButton = new Button("OK");
                okButton.setOnAction(event -> {
                    ingresar.setDisable(false);
                    dialog.hide();
                });
                dialog.getButtons().add(okButton);
                dialog.showAndWait();

            }

        } catch (Exception e) {
            e.printStackTrace();
        }


    }

    @FXML
    void register(MouseEvent event) {

        SecureRandom random = new SecureRandom();
        byte bytes[] = new byte[20];

        random.nextBytes(bytes);

        String hashSalt = bytes + pass.getText();

        System.out.println(Arrays.hashCode(bytes));

        String users = user.getText();

        try {
            SohoApp.connectivity.connectUsuarios(users, hash(hashSalt), bytes.toString());
        } catch (SQLException e) {
            e.printStackTrace();
        }

        System.out.println(hashSalt);


    }

    private void getDB() {

        Task getDB = new Task() {
            @Override
            protected Object call() throws Exception {

                SohoApp.connectivity = new Connectivity();

                SohoApp.tecnicoList = SohoApp.connectivity.getTecnicoList();

                SohoApp.clienteList = SohoApp.connectivity.getAllClientes();

                SohoApp.productoList = SohoApp.connectivity.getAllProductos();

                SohoApp.list = SohoApp.connectivity.getAllCustomer();

                return null;
            }
        };

        getDB.setOnFailed(e -> {
            getDB.getException().printStackTrace();
            // inform user of error...
        });

        getDB.setOnSucceeded(e -> {

                    // Task.getValue() gives the value returned from call()...

                    System.out.println("Get Citas Task done!");
                    progress.setVisible(false);
                    ingresar.setDisable(false);
/*
                    for (Cliente cliente : SohoApp.clienteList) {

                        citasMap.put(cliente, cliente.getNumero());

                    }*/

                }
        );


        // run the task using a thread from the thread pool:

        progress.progressProperty().bind(getDB.progressProperty());

        progress.setVisible(true);

        new Thread(getDB).start();

    }


    private String hash(String txt) {

        MessageDigest m = null;
        try {
            m = MessageDigest.getInstance("MD5");
        } catch (NoSuchAlgorithmException e) {
            e.printStackTrace();
        }
        assert m != null;
        m.reset();
        m.update(txt.getBytes());
        byte[] digest = m.digest();
        BigInteger bigInt = new BigInteger(1, digest);
        StringBuilder hashtext = new StringBuilder(bigInt.toString(16));
        // Now we need to zero pad it if you actually want the full 32 chars.

        while (hashtext.length() < 32) {
            hashtext.insert(0, "0");
        }

        return hashtext.toString();
    }


}