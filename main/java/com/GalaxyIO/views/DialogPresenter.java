package com.GalaxyIO.views;

import com.GalaxyIO.Cliente;
import com.GalaxyIO.Costumer;
import com.GalaxyIO.SohoApp;
import com.gluonhq.charm.glisten.control.Dialog;
import com.gluonhq.charm.glisten.control.DropdownButton;
import com.gluonhq.charm.glisten.control.Icon;
import com.gluonhq.charm.glisten.control.TextArea;
import com.gluonhq.charm.glisten.visual.MaterialDesignIcon;
import com.jfoenix.controls.JFXProgressBar;
import javafx.animation.PauseTransition;
import javafx.beans.value.ChangeListener;
import javafx.beans.value.ObservableValue;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.scene.Node;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.scene.control.MenuItem;
import javafx.scene.control.TextField;
import javafx.scene.input.KeyCode;
import javafx.scene.input.MouseEvent;
import javafx.scene.layout.GridPane;
import javafx.scene.layout.HBox;
import javafx.scene.layout.VBox;
import org.apache.commons.lang3.text.WordUtils;

import java.io.IOException;
import java.sql.SQLException;
import java.time.LocalDate;
import java.time.LocalDateTime;
import java.time.LocalTime;
import java.time.format.DateTimeFormatter;
import java.time.format.FormatStyle;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.concurrent.atomic.AtomicBoolean;
import java.util.concurrent.atomic.AtomicReference;

public class DialogPresenter {

    public static String nombre = null;
    public static LocalDate date = null;
    public static Dialog dialog;
    static int tiemp = 0;
    static LocalTime time1 = null;
    static ArrayList<LocalDate> dates = new ArrayList<>();
    static boolean update;
    static String eventoString;
    static String pagoString;
    static String notaString;
    private static LocalDate cumple = null;
    private static String telefono = null;
    private static String servicios = null;
    private static String tecnic = null;
    public ArrayList<Costumer> arraylist;
    @FXML
    public TextField cliente;
    @FXML
    public TextField numero;
    @FXML
    public TextField tiempoServicio;
    @FXML
    public TextField hora;
    @FXML
    public TextField tecnico;
    @FXML
    public JFXProgressBar progressBar;
    @FXML
    public TextField fecha;
    int citaIndex = 0;
    @FXML
    private TextField cumpleAnos;

    @FXML
    private TextField evento;

    @FXML
    private TextField pago;

    @FXML
    private Icon fechIcon;
    private String dbName;
    private String dbNumber;

    @FXML
    void initialize() {

        dates.clear();

        notaString = null;
        pagoString = null;
        eventoString = null;

        System.out.println("Dialog Presenter Initiated");

        progressBar.setVisible(false);

        //combo.setSelectedItem(combo.getItems().get(0));

        AtomicBoolean isCliente = new AtomicBoolean();

        AtomicReference<KeyCode> keyCode = new AtomicReference<KeyCode>();

        keyCode.set(KeyCode.ENTER);

        cliente.textProperty().addListener(new ChangeListener<String>() {
            @Override
            public void changed(ObservableValue<? extends String> observable, String oldValue, String newValue) {

                cliente.setText(WordUtils.capitalizeFully(newValue));


            }
        });

        cumpleAnos.setOnKeyPressed(event -> {

            keyCode.set(event.getCode());
            System.out.println(event.getCode());

        });

        cumpleAnos.textProperty().addListener(new ChangeListener<String>() {
            @Override
            public void changed(ObservableValue<? extends String> observable, String oldValue, String newValue) {


                if (!newValue.matches("^[0-9\\/]*$")) {
                    System.out.println("not a number: " + oldValue);

                    cumpleAnos.setText(oldValue);

                } else if (keyCode.get() != KeyCode.BACK_SPACE) {

                    if (newValue.length() > 10) {

                        cumpleAnos.setText(oldValue);

                    } else if (newValue.length() == 2 || newValue.length() == 5) {

                        cumpleAnos.setText(newValue + "/");

                    }

                }

            }
        });

        numero.textProperty().addListener(new ChangeListener<String>() {
            @Override
            public void changed(ObservableValue<? extends String> observable, String oldValue,
                                String newValue) {

                //cliente.setDisable(true);

                isCliente.set(false);

                if (!newValue.matches("\\d{0,11}")) {
                    System.out.println("not a number: " + oldValue);

                    numero.setText(oldValue);

                } else if (newValue.length() == 11) {

                    System.out.println("cell complete");

                    for (Cliente clienteLoop : SohoApp.clienteList) {

                        if (clienteLoop.getNumero().equals(newValue)) {

                            System.out.println("found cell match");
                            cliente.setText(clienteLoop.getNombre());

                            dbName = clienteLoop.getNombre();
                            dbNumber = clienteLoop.getNumero();

                            cumpleAnos.setText(String.valueOf(clienteLoop.getCumpleanos().format(DateTimeFormatter.ofPattern("dd/MM/uuuu"))));
                            //cliente.setDisable(true);
                            //isCliente.set(true);

                            break;
                        }

                    }
                }

            }
        });

    }

    public boolean btnAgregar() throws SQLException {

        arraylist = SohoApp.list;

        AtomicBoolean isDone = new AtomicBoolean(false);

        final int[] index = {0};

        if (cliente.getText().isEmpty() || cliente == null) {

            System.out.println("Cliente error");

            cliente.setOnMouseClicked(event -> {

                cliente.setStyle("");

            });

            cliente.setStyle("-fx-border-color:  ff5b5b; -fx-border-width: 2");
            index[0]++;
        } else {

            nombre = cliente.getText();

        }

        if (numero.getText().isEmpty() || numero == null) {

            numero.setOnMouseClicked(event -> {

                numero.setStyle("");

            });
            numero.setStyle("-fx-border-color:  ff5b5b; -fx-border-width: 2");
            index[0]++;
        } else {

            if (numero.getText().length() == 11) {

                telefono = numero.getText();
            }

        }

        if (tiempoServicio.getText().isEmpty() || tiempoServicio == null) {
            index[0]++;
            tiempoServicio.setStyle("-fx-border-color:  ff5b5b; -fx-border-width: 2");

            tiempoServicio.setOnMouseClicked(event -> {

                tiempoServicio.setStyle("");

            });
        } else {

            servicios = tiempoServicio.getText();

        }

        if (cumpleAnos.getText().isEmpty() || cumpleAnos == null) {
            index[0]++;

            cumpleAnos.setOnMouseClicked(event -> {

                cumpleAnos.setStyle("");

            });
            cumpleAnos.setStyle("-fx-border-color:  ff5b5b; -fx-border-width: 2");

        } else {

            cumple = LocalDate.parse(cumpleAnos.getText(), DateTimeFormatter.ofPattern("dd/MM/uuuu"));

        }

        if (tecnico.getText().isEmpty() || tecnico == null) {
            index[0]++;

            tecnico.setOnMouseClicked(event -> {

                tecnico.setStyle("");

            });
            tecnico.setStyle("-fx-border-color:  ff5b5b; -fx-border-width: 2");

        } else {

            String[] name = tecnico.getText().split("\\.");

            tecnic = name[0];

        }

        if (fecha.getText().isEmpty() || fecha == null) {
            index[0]++;

            fecha.setOnMouseClicked(event -> {

                fecha.setStyle("");

            });
            fecha.setStyle("-fx-border-color:  ff5b5b; -fx-border-width: 2");

        } else {

            if (dates.size() == 1) {

                date = dates.get(0);

                if (diaFestivo(date)) {
                    index[0]++;
                    fecha.setOnMouseClicked(event -> {

                        fecha.setStyle("");

                    });

                    fecha.setStyle("-fx-border-color:  ff5b5b; -fx-border-width: 2");

                } else if (date.getDayOfWeek().toString().equals("SUNDAY")) {

                    if(date.isEqual(LocalDate.of(2018,12,23)) || date.isEqual(LocalDate.of(2018,12,30))) {


                    }else{

                        index[0]++;
                        fecha.setOnMouseClicked(event -> {

                            fecha.setStyle("");

                        });
                        fecha.setStyle("-fx-border-color:  ff5b5b; -fx-border-width: 2");
                    }
                }

            } else {

                for (LocalDate date : dates) {

                    if (diaFestivo(date)) {
                        index[0]++;
                        fecha.setOnMouseClicked(event -> {

                            fecha.setStyle("");

                        });

                        fecha.setStyle("-fx-border-color:  ff5b5b; -fx-border-width: 2");

                    } else if (date.getDayOfWeek().toString().equals("SUNDAY")) {

                        if(!date.isEqual(LocalDate.of(2018,12,23)) || !date.isEqual(LocalDate.of(2018,12,30))) {

                            index[0]++;
                            fecha.setOnMouseClicked(event -> {

                                fecha.setStyle("");

                            });
                            fecha.setStyle("-fx-border-color:  ff5b5b; -fx-border-width: 2");

                        }
                    }
                }

            }

        }

        if (hora.getText().isEmpty() || hora == null) {
            index[0]++;

            hora.setOnMouseClicked(event -> {

                hora.setStyle("");

            });
            hora.setStyle("-fx-border-color:  ff5b5b; -fx-border-width: 2");

        } else {

            time1 = LocalTime.parse(hora.getText(), DateTimeFormatter.ofPattern("hh:mm a"));


            if (!hourDateCheck()) {//!SohoApp.connectivity.checkDate(LocalDateTime.of(fDate, time1), tecnic, citaIndex) || !SohoApp.connectivity.checkDate(LocalDateTime.of(fDate, time1.minusHours(1)), tecnic, citaIndex)) {

                hora.setOnMouseClicked(event -> {

                    hora.setStyle("");

                });
                hora.setStyle("-fx-border-color:  ff5b5b; -fx-border-width: 2");
                index[0]++;
            }
            //}
        }

        if (tiemp == 0) {

            tiemp = calendarDialog.sTiempo;

        }

        if (index[0] != 0) {
            index[0] = 0;
            isDone.set(false);

        } else {
            isDone.set(true);
        }

        return isDone.get();
    }

    private boolean diaFestivo(LocalDate date) {

        ArrayList<LocalDate> dias = new ArrayList<>();

        dias.add(LocalDate.of(LocalDate.now().getYear(), 1, 1));
        dias.add(LocalDate.of(LocalDate.now().getYear(), 2, 12));
        dias.add(LocalDate.of(LocalDate.now().getYear(), 2, 13));
        dias.add(LocalDate.of(LocalDate.now().getYear(), 3, 29));
        dias.add(LocalDate.of(LocalDate.now().getYear(), 3, 30));
        dias.add(LocalDate.of(LocalDate.now().getYear(), 4, 1));
        dias.add(LocalDate.of(LocalDate.now().getYear(), 4, 19));
        dias.add(LocalDate.of(LocalDate.now().getYear(), 5, 1));
        dias.add(LocalDate.of(LocalDate.now().getYear(), 6, 24));
        dias.add(LocalDate.of(LocalDate.now().getYear(), 7, 5));
        dias.add(LocalDate.of(LocalDate.now().getYear(), 10, 12));
        //dias.add(LocalDate.of(LocalDate.now().getYear(), 12, 25));
        dias.add(LocalDate.of(LocalDate.now().getYear(), 12, 31));


        for (LocalDate dia : dias) {

            if (date.equals(dia)) {

                return true;
            }

        }

        return false;

    }

    private boolean hourDateCheck() {

        int number = 0;

        for (Costumer costumer : SohoApp.list) {

            for (LocalDate aDate : DatePicker.dates) {

                if (tecnic.split("\\.")[0].equals(costumer.getTecnico().split("\\.")[0])
                        && costumer.getDate().toLocalDate().equals(aDate)) {

                    System.out.println("DATE AND TECNIC MATCH");

                    if (costumer.getDate().toLocalTime().equals(time1)) {

                        System.out.println("TIME OVERLAP");
                        number++;

                    } else if (costumer.getDate().toLocalTime().equals(time1.plusHours(1)) && tiemp == 2) {

                        System.out.println("TIME OVERLAP + 1");
                        number++;

                    } else if (costumer.getDate().toLocalTime().equals(time1.minusHours(1)) && costumer.getTiempoDeServ() == 2) {

                        System.out.println("TIME OVERLAP - 1");
                        number++;

                    }
                }

            }
        }

        if (update && dates.get(0).equals(LocalDate.parse(calendarDialog.sFecha, DateTimeFormatter.ofPattern("yyyy-MM-dd HH:mm")))
                && time1.equals(LocalTime.parse(calendarDialog.sFecha, DateTimeFormatter.ofPattern("yyyy-MM-dd HH:mm"))) && tecnic.split("\\.")[0].equals(calendarDialog.sTecnico.split("\\.")[0])) {

            return number - 1 == 0;


        } else {

            return number == 0;
        }
    }

    void submit() {

        try {

            if (dates.isEmpty()) {
                //arraylist.add(new Costumer(PrimaryPresenter.userID, tecnic, nombre, telefono, servicios, tiemp, LocalDateTime.of(date, time1), 0, 0));

                SohoApp.connectivity.connectCitas(PrimaryPresenter.userID, tecnic, nombre, telefono, servicios, tiemp, LocalDateTime.of(date, time1), eventoString != null ? eventoString : "", eventoString != null ? eventoString : "", notaString != null ? notaString : "");

                if ((!telefono.equals(dbNumber) && nombre.equals(dbName))
                        || (telefono.equals(dbNumber) && !nombre.equals(dbName))) {

                    SohoApp.connectivity.editCliente(nombre, telefono, dbNumber);

                    SohoApp.connectivity.updateNewClient(nombre, dbNumber, telefono);

                }

                if (fechaDiferent(telefono, cumple)) {

                    SohoApp.connectivity.birthday(cumple, telefono);

                }

                if (!isCelInDB()) {

                    SohoApp.connectivity.setNewCliente(cliente.getText(), numero.getText(), null, cumple);

                }

            } else {

                for (LocalDate date : dates) {

                    SohoApp.connectivity.connectCitas(PrimaryPresenter.userID, tecnic, nombre, telefono, servicios, tiemp, LocalDateTime.of(date, time1), eventoString != null ? eventoString : "", pagoString != null ? pagoString : "", notaString != null ? notaString : "");

                    if ((!telefono.equals(dbNumber) && nombre.equals(dbName))
                            || (telefono.equals(dbNumber) && !nombre.equals(dbName))) {

                        SohoApp.connectivity.editCliente(nombre, telefono, dbNumber);

                        SohoApp.connectivity.updateNewClient(nombre, dbNumber, telefono);

                    }

                    if (fechaDiferent(telefono, cumple)) {

                        SohoApp.connectivity.birthday(cumple, telefono);

                    }

                    if (!isCelInDB()) {

                        SohoApp.connectivity.setNewCliente(cliente.getText(), numero.getText(), null, cumple);

                    }

                }

            }

        } catch (
                SQLException e) {
            System.out.println("Appointment exists");
            e.printStackTrace();
        }

    }

    @FXML
    private void pickDate(MouseEvent event) throws IOException {

        dates.clear();

        date = null;

        Dialog dialog = new Dialog();

        FXMLLoader loader = new FXMLLoader(
                getClass().getResource(
                        "datePicker.fxml"
                )
        );

        dialog.setContent(loader.load());

        DatePicker.dialog = dialog;

        dialog.showAndWait();

        dates = ((DatePicker) loader.getController()).getDates();

        fecha.setText("");

        for (LocalDate date : dates) {

            fecha.setText(fecha.getText() + "  " + date.format(DateTimeFormatter.ofLocalizedDate(FormatStyle.SHORT)));

        }

    }

    @FXML
    private void pickHour(MouseEvent event) {

        hora.setStyle("");


        fechIcon.requestFocus();

        Dialog dialog = new Dialog();
        try {

            FXMLLoader loader = new FXMLLoader(
                    getClass().getResource(
                            "hourpicker.fxml"
                    )
            );

            dialog.setContent(loader.load());

            HourPicker.dialog = dialog;


            Button okButton = new Button("Agregar");
            okButton.setOnAction(e -> {

                HourPicker controller =
                        loader.getController();


                dialog.hide();


            });
            dialog.getButtons().add(okButton);
            dialog.showAndWait();

            LocalTime hourSplit = LocalTime.parse(HourPicker.selected.getText(),
                    DateTimeFormatter.ofPattern("hh:mm a"));

            TecnicoPicker.hour = hourSplit.getHour();

            //TecnicoPicker.tiempoServ = tiemp;

            String name = HourPicker.selected.getText();

            hora.setText(name);


            dialog.hide();


        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    @FXML
    void pickTecnico(MouseEvent event) {

        tecnico.setStyle("");


        fechIcon.requestFocus();

        Dialog dialog = new Dialog();

        FXMLLoader loader;

        try {

            loader = new FXMLLoader(
                    getClass().getResource(
                            "tecnicopicker.fxml"
                    )
            );


            dialog.setContent(loader.load());

            TecnicoPicker.dialog = dialog;

            dialog.showAndWait();

            String name = TecnicoPicker.selected.getText();

            tecnico.setText(name);

            dialog.hide();

        } catch (IOException e) {
            e.printStackTrace();
        }

    }

    @FXML
    void pickProduct(MouseEvent event) {

        tiempoServicio.setStyle("");


        fechIcon.requestFocus();

        Dialog dialog = new Dialog();

        FXMLLoader loader;

        try {

            loader = new FXMLLoader(
                    getClass().getResource(
                            "productPicker.fxml"
                    )
            );


            dialog.setContent(loader.load());

            ProductPicker.dialog = dialog;

            dialog.showAndWait();

            String name = ProductPicker.selected.getText();

            tiempoServicio.setText(name);
            tiemp = ProductPicker.tiempo;

            dialog.hide();

        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    boolean isCellUniq() throws SQLException {

        return SohoApp.connectivity.checkCell(telefono, date);
    }

    private boolean isCelInDB() {

        ArrayList<Cliente> clientearraylist = new ArrayList<>();

        for (Cliente cliente : SohoApp.clienteList) {

            if (cliente.getNumero().equals(numero.getText())) {

                clientearraylist.add(cliente);

            }

        }

        return clientearraylist.size() > 0;

    }

    public TextField getCliente() {
        return cliente;
    }

    public TextField getNumero() {
        return numero;
    }

    TextField getTiempoServicio() {
        return tiempoServicio;
    }

    TextField getHora() {
        return hora;
    }

    public TextField getTecnico() {
        return tecnico;
    }

    public TextField getFecha() {
        return fecha;
    }

    TextField getCumpleAnos() {
        return cumpleAnos;
    }

    TextField getEvento() {
        return new TextField(eventoString);
    }

    TextField getPago() {
        return new TextField(pagoString);
    }

    TextField getNota() {
        return new TextField(notaString);
    }


    void updateCita() throws SQLException {

        System.out.println(notaString);

        SohoApp.connectivity.editCita(nombre, telefono, servicios, tiemp, LocalDateTime.of(date, time1), tecnic, calendarDialog.sIndex, calendarDialog.sConfirmada, eventoString != null ? eventoString : "", pagoString != null ? pagoString : "", notaString != null ? notaString : "");

        if ((!telefono.equals(AdminPresenter.convert(calendarDialog.sNumero.split("-"))) && nombre.equals(calendarDialog.sNombre))
                || telefono.equals(AdminPresenter.convert(calendarDialog.sNumero.split("-"))) && !nombre.equals(calendarDialog.sNombre)) {

            SohoApp.connectivity.editCliente(nombre, telefono, AdminPresenter.convert(calendarDialog.sNumero.split("-")));

            SohoApp.connectivity.updateNewClient(nombre, AdminPresenter.convert(calendarDialog.sNumero.split("-")), telefono);

        } else if (!telefono.equals(AdminPresenter.convert(calendarDialog.sNumero.split("-"))) && !nombre.equals(calendarDialog.sNombre)) {

            if (!isCelInDB()) {

                SohoApp.connectivity.setNewCliente(nombre, telefono, null, cumple);

            }
        }


        if (fechaDiferent(telefono, cumple)) {

            SohoApp.connectivity.birthday(cumple, telefono);

        }


    }

    private boolean fechaDiferent(String telefono, LocalDate date) {

        for (Cliente cliente : SohoApp.clienteList) {

            if (!cliente.getCumpleanos().equals(date) && cliente.getNumero().equals(telefono)) {

                return true;

            }

        }

        return false;
    }

    @FXML
    void evento(MouseEvent event) {

        ArrayList<String> eventos = new ArrayList<>();

        String even =
                "Bomba hidratante\n" +
                        "Guantes de colageno\n" +
                        "Botines de colageno\n" +
                        "Jelly pedi\n" +
                        "Corona Happy Birthday Foil\n" +
                        "Corona Happy Birthday Black & Pink\n" +
                        "Corona Bride con Velo\n" +
                        "Banda Happy Birthday\n" +
                        "Bottom Bride\n" +
                        "Banda Bridge\n" +
                        "Cup Cake de Cumpleaños\n" +
                        "Globo de Helio\n" +
                        "Sangripops\n" +
                        "Botella Miniatura de Ron Bacardi Gold\n" +
                        "Variedad de Cerveza Mejicanas\n" +
                        "Botella Miniatura de Whisky Black Label 12 Años\n" +
                        "Botella Miniatura de Whisky Chivas Regal 18 años\n" +
                        "Copa de Sangría\n" +
                        "Buque de flores\n" +
                        "Mini Botella Champaña Codorniu";

        eventos.addAll(Arrays.asList(even.split("\n")));

        Dialog dialog = new Dialog();

        //Label selected = new Label("Elija evento");

        VBox vBox = new VBox();

        ArrayList<Button> selected = new ArrayList<Button>();

        GridPane gridPane = new GridPane();

        int column = 1;
        int row = 0;

        Button submit = new Button("Confirmar");

        submit.setOnMouseClicked(event1 -> {

            StringBuilder builder = new StringBuilder();

            for (Button b : selected) {

                builder.append(b.getText()).append(",");

                System.out.println(builder.toString());

            }

            eventoString = builder.toString();

            dialog.hide();

        });

        for (int i = 0; i < eventos.size(); i++) {

            if (i == eventos.size() / 2) {

                column = 2;
                row = 0;
            }

            Button button = new Button(eventos.get(i));

            button.setOnMouseClicked(event1 -> {

                if (button.getStyle().contains("-fx-background-color: #5e5e5e")) {

                    selected.remove(button);
                    button.setStyle(null);
                    button.setGraphic(null);


                } else if (selected.size() < 6) {

                    selected.add(button);
                    button.setStyle("-fx-background-color: #5e5e5e");
                    button.setGraphic(MaterialDesignIcon.CHECK.graphic());


                } else {

                    for (Button b : selected) {

                        b.setStyle(null);
                        b.setGraphic(null);

                    }

                    selected.clear();
                }

            });

            gridPane.add(button, column, row);

            row++;

        }

        gridPane.setVgap(10);
        gridPane.setHgap(10);

        vBox.getChildren().add(gridPane);

        vBox.getChildren().add(submit);

        vBox.setSpacing(15);

        dialog.setContent(vBox);

        if (eventoString != null && !eventoString.isEmpty()) {

            String[] eventoArray = eventoString.split(",");
            selected.clear();

            for (String evento : eventoArray) {

                for (Node node : gridPane.getChildren()) {

                    if (node.getClass().getName().equals("javafx.scene.control.Button")) {

                        if (((Button) node).getText().equals(evento)) {

                            selected.add(((Button) node));

                        }

                    }

                }

            }

            for (Button button : selected) {

                button.setStyle("-fx-background-color: #5e5e5e");
                button.setGraphic(MaterialDesignIcon.CHECK.graphic());

            }

        }

        dialog.showAndWait();

        //dialog.getContent().getScene().getWindow().sizeToScene();

    }

    @FXML
    void pago(MouseEvent event) {

        Dialog dialog = new Dialog();

        VBox vBox = new VBox();

        HBox hBox = new HBox();

        DropdownButton bancos = new DropdownButton();

        bancos.getItems().add(new MenuItem("SOHO GIFT CARD"));
        bancos.getItems().add(new MenuItem("DOLARES"));
        bancos.getItems().add(new MenuItem("BANCO BICENTENARIO"));
        bancos.getItems().add(new MenuItem("BANCO DE VENEZUELA S.A.I.C.A."));
        bancos.getItems().add(new MenuItem("BANCO DEL CARIBE C.A."));
        bancos.getItems().add(new MenuItem("BANCO EXTERIOR C.A."));
        bancos.getItems().add(new MenuItem("BANCO MERCANTIL C.A."));
        bancos.getItems().add(new MenuItem("BANCO OCCIDENTAL DE DESCUENTO."));
        bancos.getItems().add(new MenuItem("BANCO PROVINCIAL BBVA"));
        bancos.getItems().add(new MenuItem("BANESCO BANCO UNIVERSAL"));
        bancos.getItems().add(new MenuItem("FONDO COMUN"));

        hBox.getChildren().addAll(new Label("Banco: "), bancos);

        vBox.getChildren().add(hBox);

        TextField numTranfs = new TextField();

        numTranfs.setPromptText("Numero de transferencia");

        TextField monto = new TextField();

        ArrayList<PauseTransition> transitions = new ArrayList<>();

        monto.textProperty().addListener((obs, oldValue, newValue) -> {

            if (!newValue.matches("^[0-9\\/]*$")) {

                System.out.println("not a number: " + oldValue);

                monto.setText(oldValue);

            }

        });

        numTranfs.textProperty().addListener((obs, oldValue, newValue) -> {

            if (!newValue.matches("^[0-9\\/]*$")) {

                System.out.println("not a number: " + oldValue);

                numTranfs.setText(oldValue);

            }

        });

        monto.setPromptText("Monto");

        vBox.getChildren().addAll(numTranfs, monto);

        vBox.setSpacing(15);

        Button close = new Button("Confirmar");

        Button clear = new Button("Borrar");

        clear.setOnMouseClicked(event1 -> {

            MenuItem empty = new MenuItem("");

            bancos.getItems().add(empty);

            bancos.setSelectedItem(empty);
            monto.setText("");
            numTranfs.setText("");
            pagoString = null;

        });

        close.setOnMouseClicked(event1 -> {

            if (pagoString != null) {

                String regex = "(\\d)(?=(\\d{3})+$)";

                pagoString = bancos.getSelectedItem().getText() + ":" + numTranfs.getText() + ":" + monto.getText().replaceAll(regex, "$1,");

                dialog.hide();

            } else {

                dialog.hide();
            }

        });

        vBox.getChildren().add(new HBox(close, clear));

        dialog.setContent(vBox);

        if (pagoString != null && !pagoString.isEmpty()) {

            String[] pagoArray = pagoString.split(":");

            for (MenuItem item : bancos.getItems()) {

                if (item.getText().equals(pagoArray[0])) {

                    bancos.setSelectedItem(item);

                }
            }

            numTranfs.setText(pagoArray[1]);
            monto.setText(pagoArray[2].replaceAll(",", ""));


        }

        dialog.showAndWait();

    }

    @FXML
    void nota(MouseEvent event) {

        Dialog dialog = new Dialog();

        TextArea textArea = new TextArea();

        dialog.setContent(textArea);

        if (notaString != null && !notaString.isEmpty()) {

            textArea.setText(notaString);

        }

        Button guardar = new Button("Guardar");

        guardar.setOnMouseClicked(event1 -> {

            notaString = textArea.getText();
            dialog.hide();

        });

        dialog.getButtons().add(guardar);

        dialog.showAndWait();

        System.out.println(notaString);


    }

}
