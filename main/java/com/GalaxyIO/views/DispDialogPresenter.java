package com.GalaxyIO.views;


import com.gluonhq.charm.glisten.control.DatePicker;
import com.gluonhq.charm.glisten.control.Dialog;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.scene.control.Button;
import javafx.scene.input.MouseEvent;

import java.time.format.DateTimeFormatter;

public class DispDialogPresenter {

    public static Dialog dialog = new Dialog();
    @FXML
    private Button btnTecnico;
    @FXML
    private Button btnFecha;

    @FXML
    void tecnicoClicked(MouseEvent event) {

        FXMLLoader loader;
        try {

            loader = new FXMLLoader(
                    getClass().getResource(
                            "tecnicopicker.fxml"
                    )
            );


            dialog.setContent(loader.load());

            TecnicoPicker.dialog = dialog;

            dialog.showAndWait();

            String name = TecnicoPicker.selected.getText();

            btnTecnico.setText(name);

            String names[] = name.split("\\.");

            AdminPresenter.dispTecnico = names[0];


        } catch (Exception e) {


        }
    }


    @FXML
    void fechaClicked(MouseEvent event) {


        DatePicker datePicker = new DatePicker();

        datePicker.showAndWait();

        if (datePicker.getDate().getDayOfWeek().name().equals("SUNDAY")) {

            btnFecha.setText(String.valueOf(datePicker.getDate().plusDays(1).format(DateTimeFormatter.ofPattern("EEEE , dd MMMM yyyy"))));
            TecnicoPicker.date = datePicker.getDate().plusDays(1);
            AdminPresenter.dispFecha = datePicker.getDate().plusDays(1);
        } else {

            btnFecha.setText(String.valueOf(datePicker.getDate().format(DateTimeFormatter.ofPattern("EEEE , dd MMMM yyyy"))));
            TecnicoPicker.date = datePicker.getDate();
            AdminPresenter.dispFecha = datePicker.getDate();
        }


    }


}
