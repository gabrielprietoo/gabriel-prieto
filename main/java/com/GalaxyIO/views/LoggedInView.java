package com.GalaxyIO.views;

import com.gluonhq.charm.glisten.mvc.View;
import javafx.fxml.FXMLLoader;

import java.io.IOException;

public class LoggedInView {

    private final String name;

    public LoggedInView() {

        this.name = "";
    }

    public LoggedInView(String name) {
        this.name = name;
    }

    public View LoggedInView() {
        try {
            View view = FXMLLoader.load(PrimaryView.class.getResource("loggedIn.fxml"));
            return view;
        } catch (IOException e) {
            System.out.println("IOException: " + e);
            return new View();
        }
    }

}
