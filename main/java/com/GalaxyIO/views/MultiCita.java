package com.GalaxyIO.views;

import com.gluonhq.charm.glisten.control.DatePicker;
import com.gluonhq.charm.glisten.control.Dialog;
import com.gluonhq.charm.glisten.control.Icon;
import javafx.fxml.FXML;
import javafx.scene.control.TextField;
import javafx.scene.input.MouseEvent;

import java.time.LocalDate;
import java.util.ArrayList;

public class MultiCita {

    public static Dialog dialog;

    @FXML
    private Icon fechIcon;

    @FXML
    private TextField fecha;

    @FXML
    private TextField fecha1;

    @FXML
    private TextField fecha2;

    @FXML
    private TextField fecha3;

    private static ArrayList<LocalDate> localDates = new ArrayList<>();

    @FXML
    void pickDate(MouseEvent event) {

        localDates.clear();

        TextField textField = (TextField) event.getPickResult().getIntersectedNode().getParent();

        DatePicker datePicker = new DatePicker();

        fechIcon.requestFocus();

        datePicker.showAndWait();

        textField.setText(String.valueOf(datePicker.getDate()));

        System.out.println(localDates);

    }

    @FXML
    void submit(MouseEvent event) {

        if(!fecha.getText().isEmpty()){

            localDates.add(LocalDate.parse(fecha.getText()));

        }

        if(!fecha1.getText().isEmpty()){

            localDates.add(LocalDate.parse(fecha1.getText()));

        }
        if(!fecha2.getText().isEmpty()){

            localDates.add(LocalDate.parse(fecha2.getText()));

        }
        if(!fecha3.getText().isEmpty()){

            localDates.add(LocalDate.parse(fecha3.getText()));

        }

        dialog.hide();

    }
}
