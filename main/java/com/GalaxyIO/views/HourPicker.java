package com.GalaxyIO.views;

import com.gluonhq.charm.glisten.control.CardPane;
import com.gluonhq.charm.glisten.control.Dialog;
import javafx.fxml.FXML;
import javafx.geometry.Pos;
import javafx.scene.control.Button;
import javafx.scene.layout.GridPane;

import java.time.LocalDate;
import java.time.LocalTime;
import java.time.format.DateTimeFormatter;
import java.util.ArrayList;
import java.util.concurrent.atomic.AtomicBoolean;


public class HourPicker {

    public static Button selected = null;
    public static Dialog dialog;
    public static LocalDate date;
    public static String tecnico;
    @FXML
    private CardPane<Button> cardPane;
    @FXML
    private GridPane gridPane;

    @FXML
    void initialize() {

        selected = null;

        boolean registered;

        AtomicBoolean isScrolledDone = new AtomicBoolean(true);

        ArrayList<Button> bList = new ArrayList<>();


        for (int i = 8; i < 21; i++) {

            LocalTime time = LocalTime.of(i, 0);

            Button button = new Button(time.format(DateTimeFormatter.ofPattern("hh:mm a")));

            button.setCache(true);

            button.setStyle("-fx-font-size:15px;");

            button.setPrefWidth(500);

            button.setPrefHeight(1000);

            button.alignmentProperty().setValue(Pos.CENTER);

            button.setOnMouseClicked(event -> {

                if (isScrolledDone.get()) {
                    selected = button;
                    dialog.hide();
                }

            });

            registered = false;

            /*for (int j = 0; j < AdminPresenter.CalendarArraylist.size(); j++) {

                if(!registered) {

                    if (AdminPresenter.CalendarArraylist.get(j).getDate().toLocalDate().equals(date) &&
                            AdminPresenter.CalendarArraylist.get(j).getDate().getHour() == time.getHour() &&
                            AdminPresenter.CalendarArraylist.get(j).getTecnico().equals(tecnico)) {

                        registered = true;

                        button.setDisable(true);
                    }
                }

            }*/

            bList.add(button);
        }

        int index = 0;

        for (int columnIndex = 0; columnIndex < 3; columnIndex++) {

            for (int rowIndex = 0; rowIndex < bList.size()/3; rowIndex++) {

                if (index <= 12) {

                    gridPane.add(bList.get(index), columnIndex, rowIndex);

                }

                if (index <= 12) {

                    index++;

                }
            }
        }
    }

}
