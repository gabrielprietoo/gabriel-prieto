package com.GalaxyIO.views;

import com.GalaxyIO.Costumer;
import javafx.fxml.FXML;
import javafx.scene.Node;
import javafx.scene.control.Label;
import javafx.scene.control.Separator;
import javafx.scene.control.TextField;
import javafx.scene.layout.VBox;

import java.time.format.DateTimeFormatter;
import java.time.format.FormatStyle;
import java.util.ArrayList;

public class CitaPorNumero {

    @FXML
    private VBox vBox;

    @FXML
    private TextField tfNumero;

    ArrayList<Node> labels = new ArrayList<>();

    @FXML
    void initialize() {

        tfNumero.textProperty().addListener((observable, oldValue, newValue) -> {

            if (!newValue.matches("\\d{0,11}")) {
                System.out.println("not a number: " + oldValue);

                tfNumero.setText(oldValue);

            }else{

                vBox.getChildren().removeAll(labels);

                labels.clear();

                for (Costumer cita : AdminPresenter.CalendarArraylist) {

                    if (labels.size() > 100) {

                        break;

                    } else {

                        if (cita.getNumber().contains(newValue)) {

                            VBox details = new VBox();

                            details.getChildren().add(new Label("Nombre: " + cita.getName()));
                            details.getChildren().add(new Label("Tecnico: " + cita.getTecnico()));
                            details.getChildren().add(new Label("Servicio: " + cita.getServicio()));
                            details.getChildren().add(new Label("Fecha: " + cita.getDate().format(DateTimeFormatter.ofLocalizedDateTime(FormatStyle.SHORT))));

                            Separator separator = new Separator();

                            labels.add(details);
                            labels.add(separator);

                            vBox.getChildren().add(details);
                            vBox.getChildren().add(separator);

                        }

                    }
                }

            }

        });

    }
}