package com.GalaxyIO.views;

import animatefx.animation.FadeIn;
import com.GalaxyIO.*;
import com.GalaxyIO.misc.Utils;
import com.gluonhq.charm.glisten.application.MobileApplication;
import com.gluonhq.charm.glisten.control.DatePicker;
import com.gluonhq.charm.glisten.control.Dialog;
import com.gluonhq.charm.glisten.control.NavigationDrawer;
import com.gluonhq.charm.glisten.control.ProgressIndicator;
import com.gluonhq.charm.glisten.mvc.View;
import com.gluonhq.charm.glisten.visual.MaterialDesignIcon;
import javafx.animation.KeyFrame;
import javafx.animation.Timeline;
import javafx.application.Platform;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.concurrent.Task;
import javafx.event.ActionEvent;
import javafx.event.EventHandler;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.geometry.Insets;
import javafx.geometry.Pos;
import javafx.geometry.Side;
import javafx.scene.CacheHint;
import javafx.scene.Node;
import javafx.scene.control.*;
import javafx.scene.control.cell.PropertyValueFactory;
import javafx.scene.input.MouseEvent;
import javafx.scene.layout.*;
import javafx.scene.paint.Color;
import javafx.scene.paint.Paint;
import javafx.scene.shape.Line;
import javafx.scene.text.Font;
import javafx.scene.text.FontWeight;
import javafx.scene.text.Text;
import javafx.scene.text.TextAlignment;
import javafx.scene.transform.Scale;
import javafx.util.Duration;
import net.swisstech.bitly.BitlyClient;
import net.swisstech.bitly.model.Response;
import net.swisstech.bitly.model.v3.ShortenResponse;

import java.io.IOException;
import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;
import java.sql.SQLException;
import java.time.LocalDate;
import java.time.LocalDateTime;
import java.time.LocalTime;
import java.time.format.DateTimeFormatter;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Locale;
import java.util.concurrent.atomic.AtomicBoolean;

public class AdminPresenter {


    public static ArrayList<Costumer> arraylist;
    public static ArrayList<User> userArrayList;
    static LocalDate dispFecha;
    static String dispTecnico;
    static LocalDate currentPageDate;
    static ArrayList<Costumer> CalendarArraylist;
    @FXML
    Label titleText;
    @FXML
    View secondary;
    ObservableList data;
    @FXML
    Button agregarCita;
    @FXML
    Button citas;
    @FXML
    Button calendarButton;
    @FXML
    Button dispBtn;
    @FXML
    VBox calendarView;
    @FXML
    GridPane gridNode;
    @FXML
    Label date;
    @FXML
    TableView<Costumer> dispTable;
    @FXML
    ProgressIndicator PBar;
    @FXML
    Button listaDeEspera;
    @FXML
    ScrollPane scrollPane;
    @FXML
    ScrollPane scrollPane1;
    boolean nextPagina = false;
    ArrayList<Tecnico> CurrentTecnicos;
    @FXML
    private AnchorPane anchor;
    @FXML
    private Line hourLine;
    private ArrayList<Node> citasVbox;
    private boolean isDialogVisible = false;
    @FXML
    private AnchorPane hourAnchor;
    @FXML
    private StackPane stackPaneMaina;

    static String convert(String[] name) {
        StringBuilder sb = new StringBuilder();
        for (String st : name) {
            sb.append(st);
        }
        return sb.toString();
    }

    public void initialize() {

        hourAnchor.setVisible(false);

        gridNode.setCache(true);

        gridNode.setCacheHint(CacheHint.SPEED);

        Locale.setDefault(new Locale("es", "ES"));

        PBar.setVisible(false);

        createCitasTable();

        startCalendar(SohoApp.tecnicoList);

        scrollPane.setOnZoom(event -> {

            Scale newScale = new Scale();
            newScale.setPivotX(event.getSceneX());
            newScale.setPivotY(event.getSceneY());
            newScale.setX(gridNode.getScaleX() * event.getZoomFactor());
            newScale.setY(gridNode.getScaleY() * event.getZoomFactor());

            gridNode.getTransforms().add(newScale);

            event.consume();

        });

        NavigationDrawer drawer = MobileApplication.getInstance().getDrawer();

        NavigationDrawer.Item pagina = new NavigationDrawer.Item("Pagina", MaterialDesignIcon.UPDATE.graphic());

        NavigationDrawer.Item por_numero = new NavigationDrawer.Item("Cita por numero", MaterialDesignIcon.PHONE.graphic());

        NavigationDrawer.Item dias_libres = new NavigationDrawer.Item("Dias Libres", MaterialDesignIcon.PEOPLE.graphic());

        NavigationDrawer.Item cambiar_nombre = new NavigationDrawer.Item("Cambio de nombre", MaterialDesignIcon.PERSON.graphic());

        ArrayList<NavigationDrawer.Item> items = new ArrayList<>();

        items.add(pagina);
        items.add(por_numero);
        items.add(dias_libres);
        items.add(cambiar_nombre);

        Label back = new Label("ATRAS", MaterialDesignIcon.ARROW_BACK.graphic());

        back.setOnMouseClicked(event1 -> {

            drawer.getItems().clear();

            drawer.getItems().addAll(items);

        });

        pagina.setOnMouseClicked(event -> {

            drawer.getItems().clear();

            drawer.getItems().add(0, back);

            VBox box = new VBox();

            Button pagina1 = new Button("Siguiente pagina");

            pagina1.setOnMouseClicked(event1 -> {

                if (nextPagina && SohoApp.tecnicoList.size() > 21) {

                    CurrentTecnicos.clear();

                    for (int i = 20; i < SohoApp.tecnicoList.size(); i++) {

                        CurrentTecnicos.add(SohoApp.tecnicoList.get(i));
                        startCalendar(CurrentTecnicos);
                        setCalendar(CurrentTecnicos);

                    }

                } else {

                    System.out.println("List is " + SohoApp.tecnicoList.size());

                }

            });

            box.getChildren().addAll(pagina1);

            drawer.getItems().add(1, box);

            new FadeIn(drawer.getItems().get(0)).play();
            new FadeIn(drawer.getItems().get(1)).play();
        });

        por_numero.setOnMouseClicked(event -> {

            drawer.getItems().clear();

            drawer.getItems().add(0, back);

            try {
                drawer.getItems().add(1, new FXMLLoader(getClass().getResource("citaPorNumero.fxml")).load()
                );
            } catch (IOException e) {
                e.printStackTrace();
            }

            new FadeIn(drawer.getItems().get(0)).play();
            new FadeIn(drawer.getItems().get(1)).play();
        });

        dias_libres.setOnMouseClicked(event -> {

            drawer.getItems().clear();

            drawer.getItems().add(0, back);

            VBox vBox = new VBox();

            for (Tecnico tecnico : SohoApp.tecnicoList) {

                AtomicBoolean checked = new AtomicBoolean(false);

                Button button = new Button(tecnico.getNombre());

                button.setOnMouseClicked(event1 -> {

                    String date = tecnico.getDiaLibre().split(":")[0];
                    String day = null;

                    switch (date) {
                        case "MONDAY":
                            day = "L";
                            break;
                        case "TUESDAY":
                            day = "M";
                            break;
                        case "WEDNESDAY":
                            day = "MM";
                            break;
                        case "THURSDAY":
                            day = "J";
                            break;
                        case "FRIDAY":
                            day = "V";
                            break;
                        case "SATURDAY":
                            day = "S";
                            break;
                        default:
                            day = "";
                            break;
                    }

                    if (!checked.get()) {

                        day = day + ".";

                    }

                    StringBuilder stringsBuilder = new StringBuilder();

                    for (String strings : tecnico.getDiaLibre().split(":")) {

                        stringsBuilder.append(strings).append(":");

                    }

                    System.out.println(stringsBuilder.toString());

                    try {
                        SohoApp.connectivity.changeDiaLibre(day, tecnico.getNombre());
                    } catch (SQLException e) {
                        e.printStackTrace();
                    }

                    if (!checked.get()) {

                        button.setGraphic(MaterialDesignIcon.CHECK.graphic());

                    } else {

                        button.setGraphic(null);
                    }

                });

                if (tecnico.getDiaLibre().equals("NOTLIBRE")) {

                    button.setGraphic(MaterialDesignIcon.CHECK.graphic());
                    checked.set(true);

                }

                vBox.getChildren().add(button);

            }

            vBox.setSpacing(15);

            drawer.getItems().add(vBox);

          /*  try {
                drawer.getItems().add(1, new FXMLLoader(getClass().getResource("dias_libres.fxml")).load()
                );
            } catch (IOException e) {
                e.printStackTrace();
            }
*/
        });

        cambiar_nombre.setOnMouseClicked(event -> {

            drawer.getItems().clear();

            drawer.getItems().add(0, back);

            VBox vBox = new VBox();

            for (Tecnico tecnico : SohoApp.tecnicoList) {

                Button button = new Button(tecnico.getNombre());

                button.setOnMouseClicked(event1 -> {

                    drawer.getItems().clear();

                    drawer.getItems().add(back);


                });

                vBox.getChildren().add(button);

            }

            vBox.setSpacing(15);

            drawer.getItems().add(vBox);

        });

        pagina.setAutoClose(false);
        por_numero.setAutoClose(false);
        dias_libres.setAutoClose(false);
        cambiar_nombre.setAutoClose(false);

        //dias_libres.setDisable(true);

        NavigationDrawer.Header header = new NavigationDrawer.Header("OPCIONES");
        drawer.setHeader(header);

        drawer.getItems().addAll(items);

        refresher(SohoApp.tecnicoList);

    }

    ObservableList getInitialTableData(ArrayList list) {

        return FXCollections.observableList(list);
    }

    @FXML
    public void agregarBtn() {

        //javafx.scene.control.Dialog  dialog = new javafx.scene.control.Dialog();

        Dialog dialog = new Dialog();

        DialogPresenter.dialog = dialog;

        //dialog.setBackgroundFade(0.5);

        try {

            FXMLLoader loader = new FXMLLoader(
                    getClass().getResource(
                            "newCitaDialog.fxml"
                    )
            );

            dialog.setContent(loader.load());

            //stackPaneMaina.getChildren().add(dialog);

            Button okButton = new Button("Agregar");

            okButton.setOnAction(e -> {

                DialogPresenter.update = false;

                DialogPresenter controller =
                        loader.getController();

                try {
                    if (controller.btnAgregar()) {

                        if (!controller.isCellUniq()) {

                            okButton.setDisable(true);

                            Task<String> task = new Task<String>() {

                                @Override
                                protected String call() {
                                    try {
                                        controller.submit();
                                        CalendarArraylist = SohoApp.connectivity.getAllCustomer();

                                    } catch (NullPointerException e) {

                                        e.printStackTrace();
                                        return "Interrupted!";
                                    } catch (SQLException e1) {
                                        e1.printStackTrace();
                                    }

                                    return "Done!";
                                }
                            };

                            task.setOnFailed(ex -> {
                                task.getException().printStackTrace();
                                //new Thread(task).start();
                                // inform user of error...
                            });

                            task.setOnSucceeded(ex -> {

                                controller.progressBar.setVisible(false);

                                for (Node aCitasVbox : citasVbox) {

                                    gridNode.getChildren().remove(aCitasVbox);
                                }

                                citasVbox.clear();

                                setCalendar(SohoApp.tecnicoList);

                                dialog.hide();

                            });

                            if (!PBar.isVisible()) {

                                new Thread(task).start();


                            }
                            controller.progressBar.progressProperty().bind(task.progressProperty());
                            controller.progressBar.setVisible(true);

                        } else {

                            Platform.runLater(() -> {

                                Dialog dialog1 = new Dialog("Numero de telefono ocupado!", "Esta segura de hacer una cita para el mismo dia?");

                                Button si = new Button("SI");
                                Button no = new Button("NO");

                                Task<String> duplicada = new Task<String>() {

                                    @Override
                                    protected String call() {
                                        try {
                                            controller.submit();
                                            CalendarArraylist = SohoApp.connectivity.getAllCustomer();

                                        } catch (NullPointerException e) {

                                            System.out.print(e);
                                            return "Interrupted!";
                                        } catch (SQLException e1) {
                                            e1.printStackTrace();
                                        }

                                        return "Done!";
                                    }
                                };

                                duplicada.setOnFailed(ex -> {

                                    duplicada.getException().printStackTrace();
                                    // new Thread(duplicada).start();
                                    // inform user of error...
                                });

                                duplicada.setOnSucceeded(ex -> {

                                    controller.progressBar.setVisible(false);

                                    for (Node aCitasVbox : citasVbox) {

                                        gridNode.getChildren().remove(aCitasVbox);
                                    }

                                    citasVbox.clear();

                                    setCalendar(SohoApp.tecnicoList);

                                    dialog.hide();

                                });

                                si.setOnMouseClicked(event -> {

                                    if (!PBar.isVisible()) {

                                        dialog1.hide();
                                        okButton.setDisable(true);
                                        new Thread(duplicada).start();
                                        controller.progressBar.progressProperty().bind(duplicada.progressProperty());
                                        controller.progressBar.setVisible(true);

                                    }


                                });

                                no.setOnMouseClicked(event -> dialog1.hide());

                                dialog1.getButtons().add(si);

                                dialog1.getButtons().add(no);

                                dialog1.showAndWait();

                                System.out.println("CELL DECTEDTED");

                            });
                        }

                    }
                } catch (SQLException e1) {
                    e1.printStackTrace();
                }

            });

            Button cerrar = new Button("Cerrar");

            cerrar.setOnMouseClicked(event -> {

                dialog.hide();
                stackPaneMaina.setMouseTransparent(false);


            });
            //content.setActions(cerrar, okButton);
            //jfxDialog.show();

            //HBox hbox = new HBox();

            //hbox.setSpacing(25);

            //hbox.getChildren().addAll(cerrar, okButton);

            //((VBox) ((AnchorPane) dialog.getContent()).getChildren().get(0)).getChildren().add(hbox);

            dialog.getButtons().addAll(cerrar, okButton);

            //System.out.println(dialog.getContent().getStyle());

            dialog.setAutoHide(false);

            //stackPaneMaina.setMouseTransparent(true);

            //dialog.setVerticalOffset(25);

            //Platform.runLater(() -> dialog.setLayoutX((secondary.getWidth() - ((AnchorPane) dialog.getContent()).getPrefWidth()) / 2));

            dialog.showAndWait();

            TecnicoPicker.simultaneo = false;

        } catch (IOException e) {
            e.printStackTrace();
        }

    }

    @FXML
    void btnDisp() {

        Dialog dialog = new Dialog();

        FXMLLoader loader = new FXMLLoader(
                getClass().getResource(
                        "dispdialog.fxml"
                )


        );

        try {
            dialog.setContent(loader.load());
        } catch (IOException e) {
            e.printStackTrace();
        }

        Button okButton = new Button("Consultar");
        okButton.setOnAction(e -> {

            calendarView.setVisible(false);
            dispTable.setVisible(true);


            dialog.hide();

        });
        dialog.getButtons().add(okButton);
        dialog.showAndWait();

        Task<String> task = new Task<String>() {

            @Override
            protected String call() {
                try {
                    checkDisp(SohoApp.connectivity.getAllCustomer(), dispFecha, dispTecnico);
                } catch (NullPointerException e) {

                    System.out.print(e);
                    return "Interrupted!";
                } catch (SQLException e) {
                    e.printStackTrace();
                }

                return "Disponibiladad Dialog Done!";
            }
        };

        task.setOnSucceeded(e -> {

            System.out.println("Disp succed");

            disRefresh();
            PBar.setVisible(false);

        });

        if (!PBar.isVisible()) {

            new Thread(task).start();
            PBar.progressProperty().bind(task.progressProperty());
            PBar.setVisible(true);

        }


    }

    private void checkDisp(ArrayList<Costumer> list, LocalDate time, String tecnico) {

        ArrayList<Costumer> horasDeTrabajo = new ArrayList<>();

        //LocalDate time = LocalDate.now();

        if (time != null) {

            horasDeTrabajo.add(new Costumer(0, tecnico, "", "", "", 0, LocalDateTime.of(time, LocalTime.of(8, 0)), 0, null, null, null, 0));
            horasDeTrabajo.add(new Costumer(0, tecnico, "", "", "", 0, LocalDateTime.of(time, LocalTime.of(9, 0)), 0, null, null, null, 0));
            horasDeTrabajo.add(new Costumer(0, tecnico, "", "", "", 0, LocalDateTime.of(time, LocalTime.of(10, 0)), 0, null, null, null, 0));
            horasDeTrabajo.add(new Costumer(0, tecnico, "", "", "", 0, LocalDateTime.of(time, LocalTime.of(11, 0)), 0, null, null, null, 0));
            horasDeTrabajo.add(new Costumer(0, tecnico, "", "", "", 0, LocalDateTime.of(time, LocalTime.of(12, 0)), 0, null, null, null, 0));
            horasDeTrabajo.add(new Costumer(0, tecnico, "", "", "", 0, LocalDateTime.of(time, LocalTime.of(13, 0)), 0, null, null, null, 0));
            horasDeTrabajo.add(new Costumer(0, tecnico, "", "", "", 0, LocalDateTime.of(time, LocalTime.of(14, 0)), 0, null, null, null, 0));
            horasDeTrabajo.add(new Costumer(0, tecnico, "", "", "", 0, LocalDateTime.of(time, LocalTime.of(15, 0)), 0, null, null, null, 0));
            horasDeTrabajo.add(new Costumer(0, tecnico, "", "", "", 0, LocalDateTime.of(time, LocalTime.of(16, 0)), 0, null, null, null, 0));
            horasDeTrabajo.add(new Costumer(0, tecnico, "", "", "", 0, LocalDateTime.of(time, LocalTime.of(17, 0)), 0, null, null, null, 0));
            horasDeTrabajo.add(new Costumer(0, tecnico, "", "", "", 0, LocalDateTime.of(time, LocalTime.of(18, 0)), 0, null, null, null, 0));
            horasDeTrabajo.add(new Costumer(0, tecnico, "", "", "", 0, LocalDateTime.of(time, LocalTime.of(19, 0)), 0, null, null, null, 0));
            horasDeTrabajo.add(new Costumer(0, tecnico, "", "", "", 0, LocalDateTime.of(time, LocalTime.of(20, 0)), 0, null, null, null, 0));
            horasDeTrabajo.add(new Costumer(0, tecnico, "", "", "", 0, LocalDateTime.of(time, LocalTime.of(21, 0)), 0, null, null, null, 0));

            arraylist = sort(list, horasDeTrabajo);

        } else {

            System.out.println("time null");

        }


    }

    private ArrayList<Costumer> sort(ArrayList<Costumer> original, ArrayList<Costumer> newOne) {

        ArrayList<Costumer> results = new ArrayList<>();

        // Loop arrayList2 items
        for (Costumer person2 : newOne) {
            // Loop arrayList1 items
            boolean found = false;
            for (Costumer person1 : original) {
                String[] p1Tec = person1.getTecnico().split("\\.");
                String[] p2Tec = person2.getTecnico().split("\\.");


                if (person2.getDate().equals(person1.getDate()) && p2Tec[0].equals(p1Tec[0])) {

                    found = true;
                }
            }

            if (!found) {
                results.add(person2);
            }
        }

        ArrayList<Costumer> toRemove = new ArrayList<>();

        String tecnico[] = TecnicoPicker.selected.getText().split("\\.");

        for (Costumer person1 : original) {

            for (Costumer person2 : results) {

                if (person1.getTecnico().equals(tecnico[0])) {

                    LocalDateTime startDate = person1.getDate();

                    LocalDateTime endDate = person1.getDate().plusHours(person1.getTiempoDeServ());

                    if (!(person2.getDate().isBefore((startDate)) || person2.getDate().isAfter(endDate))) {

                        toRemove.add(person2);

                    }
                }

            }
        }

        results.removeAll(toRemove);

        return results;
    }

    private void getInfo() {

        Task<ArrayList<Costumer>> widgetSearchTask = new Task<ArrayList<Costumer>>() {
            @Override

            public ArrayList<Costumer> call() {

                try {
                    arraylist = SohoApp.connectivity.getAllCustomer();
                } catch (SQLException e) {
                    e.printStackTrace();
                }

                SohoApp.list = arraylist;

                CalendarArraylist = arraylist;

                PBar.setVisible(false);

                return arraylist;
            }
        };

        widgetSearchTask.setOnFailed(e -> {
            widgetSearchTask.getException().printStackTrace();
            //PBar.setVisible(false);
            // inform user of error...
        });

        if (!PBar.isVisible()) {

            new Thread(widgetSearchTask).start();
            PBar.progressProperty().bind(widgetSearchTask.progressProperty());
            PBar.setVisible(true);

        }


    }

    void populate() {

        data = getInitialTableData(arraylist);


    }

    void disRefresh() {


        TableColumn<Costumer, String> tecnico = new TableColumn<>("Tecnico");

        TableColumn<Costumer, String> fecha = new TableColumn<>("Fecha");


        data = getInitialTableData(arraylist);

        tecnico.setCellValueFactory(new PropertyValueFactory<>("tecnico"));
        fecha.setCellValueFactory(new PropertyValueFactory<>("dd"));

        dispTable.getColumns().clear();

        dispTable.getColumns().addAll(tecnico, fecha);


        dispTable.getSortOrder().setAll(Collections.singletonList(fecha));

        fecha.setSortType(TableColumn.SortType.ASCENDING);

        dispTable.setItems(data);

    }

    void updatePsw(String pass, int ID) {

        Task<String> task = new Task<String>() {

            @Override
            protected String call() {
                try {

                    SohoApp.connectivity.updatePassword(pass, ID);

                } catch (NullPointerException e) {

                    e.printStackTrace();

                    return "Interrupted!";
                } catch (SQLException e) {
                    e.printStackTrace();
                }

                return "Done!";
            }
        };

        task.setOnFailed(ex -> {
            task.getException().printStackTrace();
            // inform user of error...
        });

        task.setOnSucceeded(ex -> {


                    PBar.setVisible(false);
                    task.cancel();
                }
        );


        if (!PBar.isVisible()) {

            new Thread(task).start();
            PBar.progressProperty().bind(task.progressProperty());
            PBar.setVisible(true);

        }
    }

    public void createCitasTable() {

        //  userTableView.setVisible(false);

        //  calendarView.setVisible(false);

        arraylist = SohoApp.list;

        CalendarArraylist = arraylist;

        secondary.setShowTransitionFactory(com.gluonhq.charm.glisten.animation.FadeInUpTransition::new);

        secondary.showingProperty().addListener((obs, oldValue, newValue) -> {

            HBox.setHgrow(agregarCita, Priority.ALWAYS);
            HBox.setHgrow(citas, Priority.ALWAYS);
            HBox.setHgrow(dispBtn, Priority.ALWAYS);
            HBox.setHgrow(listaDeEspera, Priority.ALWAYS);
            HBox.setHgrow(calendarButton, Priority.ALWAYS);

            agregarCita.setPrefWidth(100000);
            citas.setPrefWidth(100000);
            dispBtn.setPrefWidth(100000);
            listaDeEspera.setPrefWidth(100000);
            calendarButton.setPrefWidth(100000);

            populate();

            getInfo();

        });

    }

    @FXML
    void calendarTable(ArrayList<Tecnico> tecnicos) {

        System.out.println("calendarTable iniciated");

        calendarView.setVisible(true);

        Task getCalendar = new Task() {
            @Override
            protected Object call() throws Exception {
                CalendarArraylist = SohoApp.connectivity.getAllCustomer();
                SohoApp.clienteList = SohoApp.connectivity.getAllClientes();
                arraylist = CalendarArraylist;
                SohoApp.list = arraylist;
                return null;
            }
        };

        getCalendar.setOnSucceeded(e -> {
            for (Node aCitasVbox : citasVbox) {

                gridNode.getChildren().remove(aCitasVbox);
            }

            citasVbox.clear();
            setCalendar(tecnicos);
            System.out.println("Refreshed calendar arraylist");

            //PBar.setVisible(false);
        });

        if (!PBar.isVisible()) {

            new Thread(getCalendar).start();
            // PBar.progressProperty().bind(getCalendar.progressProperty());
            // PBar.setVisible(true);

        }
    }

    @FXML
    void showCalendar(MouseEvent event) {

        calendarTable(SohoApp.tecnicoList);

    }

    private void startCalendar(ArrayList<Tecnico> tecnicos) {

        LocalDate localDate = LocalDate.now();

        if (localDate.getDayOfWeek().name().equals("SUNDAY")) {

            if(localDate.isEqual(LocalDate.of(2018,12,23)) || localDate.isEqual(LocalDate.of(2018,12,30))){

                date.setText(String.valueOf(localDate.format(DateTimeFormatter.ofPattern("EEEE , dd MMMM yyyy"))));

                currentPageDate = localDate;
                System.out.println(currentPageDate);

            }else{

                date.setText(String.valueOf(localDate.plusDays(1).format(DateTimeFormatter.ofPattern("EEEE , dd MMMM yyyy"))));

                currentPageDate = localDate.plusDays(1);

                System.out.println(currentPageDate);

            }

        } else {

            date.setText(String.valueOf(localDate.format(DateTimeFormatter.ofPattern("EEEE , dd MMMM yyyy"))));

            currentPageDate = localDate;
            System.out.println(currentPageDate);


        }

        citasVbox = new ArrayList<>();

        int x = 8;

        for (int i = 1; i < 15; i++) {

            StackPane stackPane = new StackPane();
            StackPane stackPane1 = new StackPane();

            stackPane.setBackground(new Background(new BackgroundFill(Color.web("#ffffff"), CornerRadii.EMPTY, new Insets(0.5, 0.5, 0.5, 0.5))));
            stackPane1.setBackground(new Background(new BackgroundFill(Color.web("#ffffff"), CornerRadii.EMPTY, new Insets(0.5, 0.5, 0.5, 0.5))));

            gridNode.add(stackPane, 7, i);

            gridNode.add(stackPane1, 15, i);

            StackPane inside = new StackPane();
            StackPane inside1 = new StackPane();

            inside.setBackground(new Background(new BackgroundFill(Color.web("#929292"), CornerRadii.EMPTY, new Insets(0.5, 0.5, 0.5, 0.5))));
            inside1.setBackground(new Background(new BackgroundFill(Color.web("#929292"), CornerRadii.EMPTY, new Insets(0.5, 0.5, 0.5, 0.5))));

            Text text = new Text();

            Text text1 = new Text();

            text1.setText(String.valueOf(x));

            text1.setFont(Font.font("Arial", FontWeight.BOLD, 28));

            text1.setFill(Paint.valueOf("#ffffff"));

            text.setText(String.valueOf(x));

            text.setFont(Font.font("Arial", FontWeight.BOLD, 28));

            text.setFill(Paint.valueOf("#ffffff"));

            inside.getChildren().add(0, text);
            inside1.getChildren().add(0, text1);

            gridNode.add(inside, 7, i);

            gridNode.add(inside1, 15, i);

            if (x == 12) {

                x = 1;
            } else {

                x++;
            }
        }

        for (int i = 0; i <= 23; i++) {

            for (int j = 0; j <= 15; j++) {

                StackPane stackPane = new StackPane();

                stackPane.setBackground(new Background(new BackgroundFill(Color.web("#ffffff"), CornerRadii.EMPTY, new Insets(0.2, 0.2, 0.2, 0.2))));

                if (getNode(gridNode, i, j) == null) {

                    gridNode.add(stackPane, i, j);
                }

            }

        }

        int k = 0;
        for (int i = 0; i <= tecnicos.size() + 1; i++) {

            if (i == 7 || i == 15) {

                i++;
            }

            StackPane stackPane = new StackPane();

            Label tecnicLabel = new Label();

            String[] text = tecnicos.get(k).getNombre().split("\\.");

            tecnicLabel.setText(text[0].toUpperCase());

            tecnicLabel.setAlignment(Pos.CENTER);

            stackPane.getChildren().add(tecnicLabel);

            StackPane.setAlignment(tecnicLabel, Pos.CENTER);

            gridNode.add(stackPane, i, 0);

            tecnicLabel.setFont(Font.font("Arial", FontWeight.BOLD, 10));

            tecnicos.get(k).setGridPlacementX(i);
            tecnicos.get(k).setGridPlacementY(0);

            k += 1;

            if (!com.gluonhq.charm.down.Platform.isIOS()) {


                Platform.runLater(() -> {

                    Double fontSize = tecnicLabel.getFont().getSize();
                    String clippedText = Utils.computeClippedText(tecnicLabel.getFont(), tecnicLabel.getText(), tecnicLabel.getWidth(), tecnicLabel.getTextOverrun(), tecnicLabel.getEllipsisString());
                    Font newFont = tecnicLabel.getFont();

                    while (!tecnicLabel.getText().equals(clippedText) && fontSize > 0.5) {
                        System.out.println("fontSize = " + fontSize + ", clippedText = " + clippedText);
                        fontSize = fontSize - 0.5;
                        newFont = Font.font(tecnicLabel.getFont().getFamily(), FontWeight.BOLD, fontSize);
                        clippedText = Utils.computeClippedText(newFont, tecnicLabel.getText(), tecnicLabel.getWidth(), tecnicLabel.getTextOverrun(), tecnicLabel.getEllipsisString());
                    }


                    tecnicLabel.setFont(newFont);

                });

            } else {

                tecnicLabel.setFont(new Font(7));

            }

        }

        setCalendar(SohoApp.tecnicoList);

    }

    void setCalendar(ArrayList<Tecnico> tecnicos) {

        Method method = null;

        int rows = 0;

        try {
            method = gridNode.getClass().getDeclaredMethod("getNumberOfRows");
        } catch (NoSuchMethodException e) {
            e.printStackTrace();
        }

        if (method != null) {
            method.setAccessible(true);
        }

        try {
            rows = (int) (method != null ? method.invoke(gridNode) : null);
            System.out.println("ROW NUMBER: " + rows);
        } catch (IllegalAccessException | InvocationTargetException e) {
            e.printStackTrace();
        }

        for (int i = 0; i < tecnicos.size(); i++) {

            LocalDate localDate = LocalDate.parse(date.getText(), DateTimeFormatter.ofPattern("EEEE , dd MMMM yyyy"));

            if (localDate.getDayOfWeek().name().equals(tecnicos.get(i).getDiaLibre()) || diaFestivo()) {

                for (int j = 1; j < rows - 1; j++) {

                    VBox libreVbox = new VBox();

                    libreVbox.setBackground(new Background(new BackgroundFill(Color.web("#d8d8d8"), CornerRadii.EMPTY, new Insets(0.5, 0.5, 0.5, 0.5))));

                    gridNode.add(libreVbox, SohoApp.tecnicoList.get(i).getGridPlacementX(), j);

                    citasVbox.add(libreVbox);

                }

            }

        }

        ArrayList<Node> Icons;

        for (int i = 0; i < CalendarArraylist.size(); i++) {

            for (int j = 0; j < tecnicos.size(); j++) {

                String[] tecnicoName = tecnicos.get(j).getNombre().split("\\.");
                String[] aTecnicoName = CalendarArraylist.get(i).getTecnico().split("\\.");

                LocalDate localDate = LocalDate.parse(date.getText(), DateTimeFormatter.ofPattern("EEEE , dd MMMM yyyy"));
                LocalDate lcDate = CalendarArraylist.get(i).getDate().toLocalDate();

                if (tecnicoName[0].equals(aTecnicoName[0]) && localDate.equals(lcDate)) {

                    int hour = CalendarArraylist.get(i).getDate().getHour();

                    int hourInt;
                    switch (hour) {
                        case 8:
                            hourInt = 1;
                            break;
                        case 9:
                            hourInt = 2;
                            break;
                        case 10:
                            hourInt = 3;
                            break;
                        case 11:
                            hourInt = 4;
                            break;
                        case 12:
                            hourInt = 5;
                            break;
                        case 13:
                            hourInt = 6;
                            break;
                        case 14:
                            hourInt = 7;
                            break;
                        case 15:
                            hourInt = 8;
                            break;
                        case 16:
                            hourInt = 9;
                            break;
                        case 17:
                            hourInt = 10;
                            break;
                        case 18:
                            hourInt = 11;
                            break;
                        case 19:
                            hourInt = 12;
                            break;
                        case 20:
                            hourInt = 13;
                            break;
                        case 21:
                            hourInt = 14;
                            break;
                        default:
                            hourInt = 0;
                            break;

                    }
                    VBox vBox = new VBox();

                    Label name = new Label();
                    Label number = new Label();

                    number.setFont(Font.font("Arial", FontWeight.BOLD, 9.0));
                    number.setWrapText(true);
                    name.setFont(Font.font("Arial", FontWeight.BOLD, 13.0));
                    name.setWrapText(true);

                    String input = CalendarArraylist.get(i).getNumber();
                    String output = input.substring(0, 4) + "-" + input.substring(4, 7) + "-" + input.substring(7, 11);

                    number.setText(output);
                    number.setTextAlignment(TextAlignment.CENTER);

                    name.setText(CalendarArraylist.get(i).getName().split(" ")[0]);
                    name.setTextAlignment(TextAlignment.CENTER);

                    vBox.getChildren().addAll(name, number);
                    vBox.setAlignment(Pos.CENTER);

                    citasVbox.add(vBox);

                    gridNode.add(vBox, tecnicos.get(j).getGridPlacementX(), hourInt);

                    HBox vBox1 = new HBox();

                    int finalI = i;

                    vBox.setOnMouseClicked((MouseEvent e) -> {

                        calendarDialog.sNombre = CalendarArraylist.get(finalI).getName();
                        calendarDialog.sFecha = CalendarArraylist.get(finalI).getDate().format(DateTimeFormatter.ofPattern("yyyy-MM-dd HH:mm"));
                        calendarDialog.sTecnico = CalendarArraylist.get(finalI).getTecnico();
                        calendarDialog.sTiempo = CalendarArraylist.get(finalI).getTiempoDeServ();
                        calendarDialog.sServicio = CalendarArraylist.get(finalI).getServicio();
                        calendarDialog.sNumero = output;
                        calendarDialog.sEvento = (CalendarArraylist.get(finalI).getEvento() != null) ? CalendarArraylist.get(finalI).getEvento() : "";
                        calendarDialog.sPago = CalendarArraylist.get(finalI).getPago() != null ? CalendarArraylist.get(finalI).getPago() : "";
                        calendarDialog.sNota = CalendarArraylist.get(finalI).getNota() != null ? CalendarArraylist.get(finalI).getNota() : "";
                        calendarDialog.sConfirmada = CalendarArraylist.get(finalI).getConfirmed();
                        calendarDialog.sIndex = CalendarArraylist.get(finalI).getIndex();

                        makeDialog(CalendarArraylist.get(finalI).getIndex(), CalendarArraylist.get(finalI).getTecnico());

                        if (isDialogVisible) {

                            Task sendSMS = new Task() {
                                @Override
                                protected Object call() throws Exception {

                                    setSMS(finalI, tecnicos);
                                    CalendarArraylist = SohoApp.connectivity.getAllCustomer();
                                    return null;
                                }
                            };

                            sendSMS.setOnSucceeded(event1 -> {


                                for (Node aCitasVbox : citasVbox) {

                                    gridNode.getChildren().remove(aCitasVbox);
                                }

                                citasVbox.clear();

                                setCalendar(SohoApp.tecnicoList);

                                PBar.setVisible(false);
                            });

                            sendSMS.setOnFailed(event -> new Thread(sendSMS).start());

                            if (!PBar.isVisible() && CalendarArraylist.get(finalI).getConfirmed() == 0) {

                                new Thread(sendSMS).start();
                                PBar.progressProperty().bind(sendSMS.progressProperty());
                                PBar.setVisible(true);
                                // vBox.setBackground(new Background(new BackgroundFill(Color.web("#7ac1ff"), CornerRadii.EMPTY, new Insets(2, 2, 2, 2))));


                            } else if (!PBar.isVisible() && CalendarArraylist.get(finalI).getConfirmed() == 1) {

                                Dialog mConfirmDialog = new Dialog("Confirmación manual", "Desea confirmar manualmente esta cita?");

                                Button si = new Button("SI");
                                Button no = new Button("NO");

                                Task manualConfirmed = new Task() {
                                    @Override
                                    protected Object call() throws Exception {

                                        SohoApp.connectivity.manualConfirm(CalendarArraylist.get(finalI).getIndex());
                                        CalendarArraylist = SohoApp.connectivity.getAllCustomer();
                                        return null;
                                    }
                                };

                                manualConfirmed.setOnSucceeded(event -> {

                                    mConfirmDialog.hide();

                                    for (Node aCitasVbox : citasVbox) {

                                        gridNode.getChildren().remove(aCitasVbox);
                                    }

                                    citasVbox.clear();

                                    setCalendar(SohoApp.tecnicoList);

                                });

                                si.setOnMouseClicked(event -> new Thread(manualConfirmed).start());

                                no.setOnMouseClicked(event -> mConfirmDialog.hide());

                                mConfirmDialog.getButtons().setAll(si, no);

                                mConfirmDialog.showAndWait();
                            }


                        }

                    });

                    //new FadeInDown(vBox).play();
                    //new FadeInDown(vBox1).play();
                    vBox1.setAlignment(Pos.CENTER);
                    ArrayList<Node> icons = new ArrayList<Node>();


                    if (CalendarArraylist.get(i).getTiempoDeServ() == 2) {

                        gridNode.add(vBox1, tecnicos.get(j).getGridPlacementX(), hourInt + 1);

                        if (CalendarArraylist.get(i).getEvento() != null) {

                            if (!CalendarArraylist.get(i).getEvento().isEmpty()) {

                                Node icon = MaterialDesignIcon.STAR.graphic();

                                vBox1.getChildren().add(icon);
                                icons.add(icon);

                            }
                        }

                        if (CalendarArraylist.get(i).getPago() != null) {

                            if (!CalendarArraylist.get(i).getPago().isEmpty()) {

                                Node icon = MaterialDesignIcon.PAYMENT.graphic();

                                vBox1.getChildren().add(icon);
                                icons.add(icon);
                            }
                        }

                        if (CalendarArraylist.get(i).getNota() != null) {

                            if (!CalendarArraylist.get(i).getNota().isEmpty()) {

                                Node icon = MaterialDesignIcon.PAGES.graphic();

                                vBox1.getChildren().add(icon);
                                icons.add(icon);
                            }
                        }

                        if (CalendarArraylist.get(i).getConfirmed() == 4) {

                            vBox1.setBackground(new Background(new BackgroundFill(Color.web("#e99bff"), CornerRadii.EMPTY, new Insets(0, 2, 2, 2))));

                            VBox node = (VBox) getNode(gridNode, tecnicos.get(j).getGridPlacementX(), hourInt);

                            node.setBackground(new Background(new BackgroundFill(Color.web("#e99bff"), CornerRadii.EMPTY, new Insets(2, 2, 0, 2))));

                        } else if (CalendarArraylist.get(i).getConfirmed() == 3) {

                            vBox1.setBackground(new Background(new BackgroundFill(Color.web("#ffe500"), CornerRadii.EMPTY, new Insets(0, 2, 2, 2))));

                            VBox node = (VBox) getNode(gridNode, tecnicos.get(j).getGridPlacementX(), hourInt);

                            node.setBackground(new Background(new BackgroundFill(Color.web("#ffe500"), CornerRadii.EMPTY, new Insets(2, 2, 0, 2))));

                        } else if (CalendarArraylist.get(i).getConfirmed() == 2) {

                            vBox1.setBackground(new Background(new BackgroundFill(Color.web("#8eff8c"), CornerRadii.EMPTY, new Insets(0, 2, 2, 2))));

                            VBox node = (VBox) getNode(gridNode, tecnicos.get(j).getGridPlacementX(), hourInt);

                            node.setBackground(new Background(new BackgroundFill(Color.web("#8eff8c"), CornerRadii.EMPTY, new Insets(2, 2, 0, 2))));
                        } else if (CalendarArraylist.get(i).getConfirmed() == 1) {

                            vBox1.setBackground(new Background(new BackgroundFill(Color.web("#7ac1ff"), CornerRadii.EMPTY, new Insets(0, 2, 2, 2))));

                            VBox node = (VBox) getNode(gridNode, tecnicos.get(j).getGridPlacementX(), hourInt);

                            node.setBackground(new Background(new BackgroundFill(Color.web("#7ac1ff"), CornerRadii.EMPTY, new Insets(2, 2, 0, 2))));

                        } else {

                            vBox1.setBackground(new Background(new BackgroundFill(Color.web("#ffa0a0"), CornerRadii.EMPTY, new Insets(0, 2, 2, 2))));

                            VBox node = (VBox) getNode(gridNode, tecnicos.get(j).getGridPlacementX(), hourInt);

                            node.setBackground(new Background(new BackgroundFill(Color.web("#ffa0a0"), CornerRadii.EMPTY, new Insets(2, 2, 0, 2))));

                        }

                        for (Node Icon : icons) {

                            Icon.setScaleX(0.6);
                            Icon.setScaleY(0.6);

                        }

                        vBox1.setOnMouseClicked(e -> {

                            calendarDialog.sNombre = CalendarArraylist.get(finalI).getName();
                            calendarDialog.sFecha = CalendarArraylist.get(finalI).getDate().format(DateTimeFormatter.ofPattern("yyyy-MM-dd HH:mm"));
                            calendarDialog.sTecnico = CalendarArraylist.get(finalI).getTecnico();
                            calendarDialog.sTiempo = CalendarArraylist.get(finalI).getTiempoDeServ();
                            calendarDialog.sServicio = CalendarArraylist.get(finalI).getServicio();
                            calendarDialog.sNumero = output;
                            calendarDialog.sEvento = CalendarArraylist.get(finalI).getEvento() != null ? CalendarArraylist.get(finalI).getEvento() : "";
                            calendarDialog.sPago = CalendarArraylist.get(finalI).getPago() != null ? CalendarArraylist.get(finalI).getPago() : "";
                            calendarDialog.sNota = CalendarArraylist.get(finalI).getNota() != null ? CalendarArraylist.get(finalI).getNota() : "";
                            calendarDialog.sConfirmada = CalendarArraylist.get(finalI).getConfirmed();
                            calendarDialog.sIndex = CalendarArraylist.get(finalI).getIndex();

                            makeDialog(CalendarArraylist.get(finalI).getIndex(), CalendarArraylist.get(finalI).getTecnico());

                            if (isDialogVisible) {

                                Task sendSMS = new Task() {
                                    @Override
                                    protected Object call() throws Exception {

                                        setSMS(finalI, tecnicos);
                                        CalendarArraylist = SohoApp.connectivity.getAllCustomer();
                                        return null;
                                    }
                                };

                                sendSMS.setOnSucceeded(event1 -> {


                                    for (Node aCitasVbox : citasVbox) {

                                        gridNode.getChildren().remove(aCitasVbox);
                                    }

                                    citasVbox.clear();

                                    setCalendar(SohoApp.tecnicoList);

                                    PBar.setVisible(false);
                                });

                                sendSMS.setOnFailed(event -> new Thread(sendSMS).start());

                                if (!PBar.isVisible() && CalendarArraylist.get(finalI).getConfirmed() == 0) {

                                    new Thread(sendSMS).start();
                                    PBar.progressProperty().bind(sendSMS.progressProperty());
                                    PBar.setVisible(true);
                                    // vBox.setBackground(new Background(new BackgroundFill(Color.web("#7ac1ff"), CornerRadii.EMPTY, new Insets(2, 2, 2, 2))));


                                } else if (!PBar.isVisible() && CalendarArraylist.get(finalI).getConfirmed() == 1) {

                                    Dialog mConfirmDialog = new Dialog("Confirmación manual", "Desea confirmar manualmente esta cita?");

                                    Button si = new Button("SI");
                                    Button no = new Button("NO");

                                    Task manualConfirmed = new Task() {
                                        @Override
                                        protected Object call() throws Exception {

                                            SohoApp.connectivity.manualConfirm(CalendarArraylist.get(finalI).getIndex());
                                            CalendarArraylist = SohoApp.connectivity.getAllCustomer();
                                            return null;
                                        }
                                    };

                                    manualConfirmed.setOnSucceeded(event -> {

                                        mConfirmDialog.hide();

                                        for (Node aCitasVbox : citasVbox) {

                                            gridNode.getChildren().remove(aCitasVbox);
                                        }

                                        citasVbox.clear();

                                        setCalendar(SohoApp.tecnicoList);

                                    });

                                    si.setOnMouseClicked(event -> new Thread(manualConfirmed).start());

                                    no.setOnMouseClicked(event -> mConfirmDialog.hide());

                                    mConfirmDialog.getButtons().setAll(si, no);

                                    mConfirmDialog.showAndWait();
                                }


                            }

                        });

                        citasVbox.add(vBox1);
                    } else {
                        vBox.setAlignment(Pos.CENTER);

                        if (CalendarArraylist.get(i).getEvento() != null) {

                            if (!CalendarArraylist.get(i).getEvento().isEmpty()) {

                                Node icon = MaterialDesignIcon.STAR.graphic();

                                vBox.getChildren().add(icon);
                                icons.add(icon);
                            }
                        }
                        if (CalendarArraylist.get(i).getPago() != null) {

                            if (!CalendarArraylist.get(i).getPago().isEmpty()) {

                                Node icon = MaterialDesignIcon.PAYMENT.graphic();

                                vBox.getChildren().add(icon);
                                icons.add(icon);

                            }
                        }

                        if (CalendarArraylist.get(i).getNota() != null) {

                            if (!CalendarArraylist.get(i).getNota().isEmpty()) {

                                Node icon = MaterialDesignIcon.PAGES.graphic();

                                vBox.getChildren().add(icon);
                                icons.add(icon);

                            }
                        }

                        if (CalendarArraylist.get(i).getConfirmed() == 4) {

                            vBox.setBackground(new Background(new BackgroundFill(Color.web("#e99bff"), CornerRadii.EMPTY, new Insets(2, 2, 2, 2))));


                        } else if (CalendarArraylist.get(i).getConfirmed() == 3) {

                            vBox.setBackground(new Background(new BackgroundFill(Color.web("#ffe500"), CornerRadii.EMPTY, new Insets(2, 2, 2, 2))));


                        } else if (CalendarArraylist.get(i).getConfirmed() == 2) {

                            vBox.setBackground(new Background(new BackgroundFill(Color.web("#8eff8c"), CornerRadii.EMPTY, new Insets(2, 2, 2, 2))));


                        } else if (CalendarArraylist.get(i).getConfirmed() == 1) {

                            vBox.setBackground(new Background(new BackgroundFill(Color.web("#7ac1ff"), CornerRadii.EMPTY, new Insets(2, 2, 2, 2))));


                        } else {

                            VBox node = (VBox) getNode(gridNode, tecnicos.get(j).getGridPlacementX(), hourInt);

                            node.setBackground(new Background(new BackgroundFill(Color.web("#ffa0a0"), CornerRadii.EMPTY, new Insets(2, 2, 2, 2))));

                        }

                        for (Node Icon : icons) {

                            Icon.setScaleX(0.6);
                            Icon.setScaleY(0.6);

                        }

                    }

                }

            }

        }

    }

    private boolean diaFestivo() {

        ArrayList<LocalDate> dias = new ArrayList<>();

        dias.add(LocalDate.of(LocalDate.now().getYear(), 1, 1));
        dias.add(LocalDate.of(LocalDate.now().getYear(), 2, 12));
        dias.add(LocalDate.of(LocalDate.now().getYear(), 2, 13));
        dias.add(LocalDate.of(LocalDate.now().getYear(), 3, 29));
        dias.add(LocalDate.of(LocalDate.now().getYear(), 3, 30));
        dias.add(LocalDate.of(LocalDate.now().getYear(), 4, 1));
        dias.add(LocalDate.of(LocalDate.now().getYear(), 4, 19));
        dias.add(LocalDate.of(LocalDate.now().getYear(), 5, 1));
        dias.add(LocalDate.of(LocalDate.now().getYear(), 6, 24));
        dias.add(LocalDate.of(LocalDate.now().getYear(), 7, 5));
        dias.add(LocalDate.of(LocalDate.now().getYear(), 7, 24));
        dias.add(LocalDate.of(LocalDate.now().getYear(), 10, 12));
        //dias.add(LocalDate.of(LocalDate.now().getYear(), 12, 25));
        dias.add(LocalDate.of(LocalDate.now().getYear(), 12, 31));


        for (LocalDate dia : dias) {

            if (currentPageDate.equals(dia)) {

                return true;
            }

        }

        return false;

    }

    private Node getNode(GridPane grid, int column, int row) {
        Node result = null;
        for (Node node : grid.getChildren()) {

            if (GridPane.getColumnIndex(node) == column && GridPane.getRowIndex(node) == row) {

                result = node;
            }


        }
        return result;

    }

    @FXML
    void FowardCalendar(MouseEvent event) {

        LocalDate localDate = LocalDate.parse(date.getText(), DateTimeFormatter.ofPattern("EEEE , dd MMMM yyyy"));

        if (localDate.plusDays(1).getDayOfWeek().name().equals("SUNDAY")) {

            if (localDate.plusDays(1).isEqual(LocalDate.of(2018, 12, 23)) || localDate.plusDays(1).isEqual(LocalDate.of(2018, 12, 30))) {

                localDate = localDate.plusDays(1);

            } else {

                localDate = localDate.plusDays(2);

            }


        } else {

            localDate = localDate.plusDays(1);

        }

        currentPageDate = localDate;
        System.out.println(currentPageDate);

        date.setText(String.valueOf(localDate.format(DateTimeFormatter.ofPattern("EEEE , dd MMMM yyyy"))));

        for (Node aCitasVbox : citasVbox) {

            gridNode.getChildren().remove(aCitasVbox);
        }

        citasVbox.clear();

        setCalendar(SohoApp.tecnicoList);

    }

    @FXML
    void backCalendar(MouseEvent event) {

        LocalDate localDate = LocalDate.parse(date.getText(), DateTimeFormatter.ofPattern("EEEE , dd MMMM yyyy"));

        if (localDate.minusDays(1).getDayOfWeek().name().equals("SUNDAY")) {

            if (localDate.minusDays(1).isEqual(LocalDate.of(2018, 12, 23)) || localDate.minusDays(1).isEqual(LocalDate.of(2018, 12, 30))) {

                localDate = localDate.minusDays(1);

            } else {

                localDate = localDate.minusDays(2);

            }


        } else {

            localDate = localDate.minusDays(1);

        }

        currentPageDate = localDate;
        System.out.println(currentPageDate);

        date.setText(String.valueOf(localDate.format(DateTimeFormatter.ofPattern("EEEE , dd MMMM yyyy"))));

        for (Node aCitasVbox : citasVbox) {

            gridNode.getChildren().remove(aCitasVbox);
        }

        citasVbox.clear();

        setCalendar(SohoApp.tecnicoList);

    }

    public void makeDialog(int index, String tecnico) {

        isDialogVisible = false;

        Dialog dialog = new Dialog();

        FXMLLoader loader = new FXMLLoader(getClass().getResource(
                "calendarDialog.fxml"
        ));

        try {
            dialog.setContent(loader.load());
        } catch (IOException e) {
            e.printStackTrace();
        }

        Button confirmar = new Button("Confirmar");
        Button cancelar = new Button("Eliminar");
        Button atendida = new Button("Atendida");
        Button editar = new Button("Editar");

        dialog.getButtons().add(cancelar);
        dialog.getButtons().add(editar);
        dialog.getButtons().add(atendida);
        dialog.getButtons().add(confirmar);

        editar.setOnMouseClicked(event -> {

            Dialog editarDialog = new Dialog();

            FXMLLoader editarLoader = new FXMLLoader(getClass().getResource(
                    "newCitaDialog.fxml"
            ));

            try {
                editarDialog.setContent(editarLoader.load());
            } catch (IOException e) {
                e.printStackTrace();
            }

            Button editarCancel = new Button("Cancelar");
            Button editarConfirmar = new Button("Confirmar");

            editarCancel.setOnMouseClicked(event1 -> editarDialog.hide());

            editarDialog.getButtons().add(editarCancel);
            editarDialog.getButtons().add(editarConfirmar);

            DialogPresenter controller = editarLoader.getController();

            editarConfirmar.setOnMouseClicked(event1 -> {

                DialogPresenter.update = true;

                System.out.println("Dialog.Presenter DATES.SIZE = " + com.GalaxyIO.views.DatePicker.dates.size());

                try {
                    if (controller.btnAgregar()) {

                        Task<String> update = new Task<String>() {

                            @Override
                            protected String call() {
                                try {
                                    controller.updateCita();
                                    CalendarArraylist = SohoApp.connectivity.getAllCustomer();
                                } catch (NullPointerException e) {

                                    System.out.print(e);
                                    return "Interrupted!";
                                } catch (SQLException e) {
                                    e.printStackTrace();
                                }
                                return "Done!";
                            }
                        };

                        update.setOnFailed(event2 -> {

                            //new Thread(update).start();

                        });

                        update.setOnSucceeded(event3 -> {


                            for (Node aCitasVbox : citasVbox) {

                                gridNode.getChildren().remove(aCitasVbox);
                            }

                            citasVbox.clear();

                            setCalendar(SohoApp.tecnicoList);

                            controller.progressBar.setVisible(false);
                            editarDialog.hide();
                            dialog.hide();

                        });


                        if (!update.isRunning()) {
                            new Thread(update).start();
                            controller.progressBar.progressProperty().bind(update.progressProperty());
                            controller.progressBar.setVisible(true);

                        }

                    }
                } catch (SQLException e) {
                    e.printStackTrace();
                }
            });

            controller.getNumero().setText(convert(calendarDialog.sNumero.split("-")));

            controller.getCliente().setText(calendarDialog.sNombre);

            DialogPresenter.pagoString = calendarDialog.sPago != null ? calendarDialog.sPago : "";
            DialogPresenter.eventoString = calendarDialog.sEvento != null ? calendarDialog.sEvento : "";
            DialogPresenter.notaString = calendarDialog.sNota != null ? calendarDialog.sNota : "";


            //controller.getServicio().setText(calendarDialog.sServicio);

            controller.getTiempoServicio().setText(calendarDialog.sServicio);

            DialogPresenter.date = LocalDate.parse(calendarDialog.sFecha, DateTimeFormatter.ofPattern("yyyy-MM-dd HH:mm"));

            TecnicoPicker.date = LocalDate.parse(calendarDialog.sFecha, DateTimeFormatter.ofPattern("yyyy-MM-dd HH:mm"));

            controller.citaIndex = calendarDialog.sIndex;

            DialogPresenter.tiemp = calendarDialog.sTiempo;

            controller.getFecha().setText(String.valueOf(LocalDate.parse(calendarDialog.sFecha, DateTimeFormatter.ofPattern("yyyy-MM-dd HH:mm")).format(DateTimeFormatter.ofPattern("EEEE , dd MMMM yyyy"))));

            for (Cliente cliente : SohoApp.clienteList) {

                if (convert(calendarDialog.sNumero.split("-")).equals(cliente.getNumero())) {

                    controller.getCumpleAnos().setText(cliente.getCumpleanos().format(DateTimeFormatter.ofPattern("dd/MM/uuuu")));

                }

            }


            LocalTime localTime = LocalTime.parse(calendarDialog.sFecha, DateTimeFormatter.ofPattern("yyyy-MM-dd HH:mm"));

            controller.getHora().setText(String.valueOf(localTime.format(DateTimeFormatter.ofPattern("hh:mm a"))));

            controller.getTecnico().setText(calendarDialog.sTecnico);

            TecnicoPicker.hour = localTime.getHour();

            TecnicoPicker.date = LocalDate.parse(calendarDialog.sFecha, DateTimeFormatter.ofPattern("yyyy-MM-dd HH:mm"));

            DialogPresenter.time1 = localTime;

            com.GalaxyIO.views.DatePicker.dates.clear();

            com.GalaxyIO.views.DatePicker.dates.add(TecnicoPicker.date);

            DialogPresenter.dates.clear();

            DialogPresenter.dates.add(TecnicoPicker.date);

            editarDialog.setAutoHide(false);

            editarDialog.showAndWait();

        });

        confirmar.setOnAction(event -> {

            isDialogVisible = true;
            dialog.hide();

        });

        cancelar.setOnAction(event -> {

            Dialog areYouSureDialog = new Dialog("Eliminar Cita", "Esta seguro?");

            Button seguro = new Button("Confirmar");
            Button notSure = new Button("Cancelar");

            areYouSureDialog.getButtons().add(notSure);

            areYouSureDialog.getButtons().add(seguro);

            notSure.setOnAction(event1 -> areYouSureDialog.hide());

            seguro.setOnAction(e -> {

                Task task = new Task() {
                    @Override
                    protected Object call() {
                        try {
                            SohoApp.connectivity.deleteCustumer(index);
                            CalendarArraylist = SohoApp.connectivity.getAllCustomer();
                        } catch (SQLException e1) {
                            e1.printStackTrace();
                        }
                        return null;
                    }
                };

                task.setOnFailed(event1 -> {

                    //new Thread(task).start();

                });

                task.setOnSucceeded(event1 -> {


                    for (Node aCitasVbox : citasVbox) {

                        gridNode.getChildren().remove(aCitasVbox);
                    }

                    citasVbox.clear();

                    setCalendar(SohoApp.tecnicoList);
                    PBar.setVisible(false);

                });

                if (!PBar.isVisible()) {

                    new Thread(task).start();
                    PBar.progressProperty().bind(task.progressProperty());
                    PBar.setVisible(true);
                    dialog.hide();
                    areYouSureDialog.hide();

                }

            });

            areYouSureDialog.showAndWait();

        });

        atendida.setOnMouseClicked(event -> {

            Task atender = new Task() {
                @Override
                protected Object call() throws Exception {

                    SohoApp.connectivity.setAtendida(index);
                    CalendarArraylist = SohoApp.connectivity.getAllCustomer();

                    return null;
                }
            };

            atender.setOnFailed(event1 -> {

                //new Thread(atender).start();

            });

            atender.setOnSucceeded(event1 -> {

                for (Node aCitasVbox : citasVbox) {

                    gridNode.getChildren().remove(aCitasVbox);
                }

                citasVbox.clear();

                setCalendar(SohoApp.tecnicoList);
                PBar.setVisible(false);
            });

            if (!PBar.isVisible()) {
                new Thread(atender).start();
                PBar.progressProperty().bind(atender.progressProperty());
                PBar.setVisible(true);
                dialog.hide();
            }


        });

        dialog.showAndWait();

        calendarDialog.clearAll();

    }

    @FXML
    void calendarPicker(MouseEvent event) {

        DatePicker datePicker = new DatePicker(LocalDate.parse(date.getText(), DateTimeFormatter.ofPattern("EEEE , dd MMMM yyyy")));

        datePicker.showAndWait();

        LocalDate localDate = datePicker.getDate();

        if (localDate.getDayOfWeek().name().equals("SUNDAY")) {

            if(localDate.isEqual(LocalDate.of(2018,12,23)) || localDate.isEqual(LocalDate.of(2018,12,30))){

                date.setText(String.valueOf(localDate.format(DateTimeFormatter.ofPattern("EEEE , dd MMMM yyyy"))));

            }else{

                date.setText(String.valueOf(localDate.plusDays(1).format(DateTimeFormatter.ofPattern("EEEE , dd MMMM yyyy"))));

            }
        } else {

            date.setText(String.valueOf(localDate.format(DateTimeFormatter.ofPattern("EEEE , dd MMMM yyyy"))));

        }

        for (Node aCitasVbox : citasVbox) {

            gridNode.getChildren().remove(aCitasVbox);
        }

        citasVbox.clear();

        setCalendar(SohoApp.tecnicoList);

    }

    private void refresher(ArrayList<Tecnico> tecnicos) {

        Timeline fiveSecondsWonder = new Timeline(new KeyFrame(Duration.seconds(15), new EventHandler<ActionEvent>() {

            @Override
            public void handle(ActionEvent event) {

                if (calendarView.isVisible()) {

                    // double offset = getNode(gridNode, 0, 0).getLayoutBounds().getHeight();

                    //double position = ((9 * gridNode.getHeight()) / 64) + offset;
                    //hourLine.setLayoutY(position);
                    //System.out.println(position);
                    System.out.println(gridNode.getHeight());
                    calendarTable(tecnicos);
                    System.out.println("Refreshed interval completed");
                }

            }
        }));
        fiveSecondsWonder.setCycleCount(Timeline.INDEFINITE);
        fiveSecondsWonder.play();
    }

    public boolean isCellUniq(DatePicker datePicker) {

        for (int i = 0; i < SohoApp.list.size(); i++) {

            String input = SohoApp.list.get(i).getNumber();
            String output = input.substring(0, 4) + "-" + input.substring(4, 7) + "-" + input.substring(7, 11);

            if (output.equals(calendarDialog.sNumero)) {

                if (SohoApp.list.get(i).getDate().toLocalDate().equals(datePicker.getDate()) && SohoApp.list.get(i).getIndex() != calendarDialog.sIndex) {
                    return false;
                }

            }


        }

        return true;
    }

    private void setSMS(int finalI, ArrayList<Tecnico> tecnicos) throws SQLException {

        BitlyClient client = new BitlyClient("58567062bcb8e4b9115f92015a22c45efd0d5687");

        Response<ShortenResponse> respShort = client.shorten() //
                .setLongUrl("google.com")
                .call();

        respShort = client.shorten() //
                .setLongUrl("http://sohonails.com.ve/test.php?index=" + CalendarArraylist.get(finalI).getIndex())
                .call();

        //String msgg = "Gabriel Prieto, Soho Nails te recuerda tu cita  mañana a las 9:00 am. Para Confirmar tu asistencia  haz click aquí";

        String msg = arraylist.get(finalI).getName() +
                ", Soho Nails te recuerda tu cita  mañana a las " + arraylist.get(finalI).getDate().toLocalTime().format(DateTimeFormatter.ofPattern("hh:mm a"))
                + ". Para onfirmar tu asistencia haz click aquí:" + respShort.data.url;

        if (Connectivity.sendSMS(arraylist.get(finalI).getNumber(), arraylist.get(finalI).getName(), msg)) {

            SohoApp.connectivity.setConfirmed(CalendarArraylist.get(finalI).getIndex());

            System.out.println(msg);
            calendarTable(tecnicos);

        }

    }

    @FXML
    void confirmAll(MouseEvent event) {

        Dialog dialog = new Dialog();

        FXMLLoader loader = new FXMLLoader(
                getClass().getResource(
                        "confirmarTodas.fxml"
                ));

        try {
            dialog.setContent(loader.load());
        } catch (IOException e) {
            e.printStackTrace();
        }

        ConfirmTodas.dialog = dialog;

        dialog.showAndWait();

    }

    @FXML
    void getListaEspera(MouseEvent event) throws IOException {

        Dialog dialog = new Dialog();

        FXMLLoader loader = new FXMLLoader(
                getClass().getResource(
                        "listaEspera.fxml"
                )
        );

        dialog.setContent(loader.load());

        dialog.showAndWait();


    }

    @FXML
    void settings(MouseEvent event) {

        System.out.println("Options");

        // Get the drawer:
        NavigationDrawer drawer = MobileApplication.getInstance().getDrawer();

        drawer.setSide(Side.RIGHT);

        drawer.open();
    }

}