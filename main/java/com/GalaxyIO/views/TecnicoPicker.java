package com.GalaxyIO.views;

import com.GalaxyIO.SohoApp;
import com.GalaxyIO.Tecnico;
import com.gluonhq.charm.glisten.control.CardPane;
import com.gluonhq.charm.glisten.control.Dialog;
import com.gluonhq.charm.glisten.control.Icon;
import com.gluonhq.charm.glisten.visual.MaterialDesignIcon;
import javafx.fxml.FXML;
import javafx.geometry.Pos;
import javafx.scene.control.Button;
import javafx.scene.control.ScrollPane;
import javafx.scene.layout.GridPane;

import java.time.LocalDate;
import java.util.ArrayList;
import java.util.concurrent.atomic.AtomicReference;

public class TecnicoPicker {

    public static Button selected = null;
    public static Dialog dialog;
    public static LocalDate date;
    public static int hour;
    public static int tiempoServ;
    public static boolean simultaneo = false;
    @FXML
    private CardPane<Button> cardPane;
    @FXML
    private ScrollPane sp;
    @FXML
    private GridPane gridPane;

    ArrayList<Button> selectedButtons = new ArrayList<>();

    public void initialize() {

        selected = null;

        ArrayList<Tecnico> tecnicos = SohoApp.tecnicoList;

        ArrayList<Button> bList = new ArrayList<>();


        for (int i = 0; i < tecnicos.size(); i++) {

            if (date == null) {

                date = LocalDate.now();

            }

            Button tecnico = new Button();

            tecnico.setText(tecnicos.get(i).getNombre());

            tecnico.setStyle("-fx-font-size:15px;");

            tecnico.setPrefWidth(500);

            tecnico.setPrefHeight(500);

            tecnico.setCache(true);

            tecnico.alignmentProperty().setValue(Pos.CENTER);

            if (simultaneo) {

                AtomicReference<Boolean> isSelected = new AtomicReference<>(false);

                tecnico.setOnMouseClicked(e -> {

                    if(!isSelected.get() && selectedButtons.size() < 2){

                        selectedButtons.add(tecnico);
                        tecnico.setGraphic(new Icon(MaterialDesignIcon.CHECK_BOX));

                        if(selectedButtons.size() == 2){

                            Dialog dialog = new Dialog("Confirme!", ("Desea seleccionar a: " + selectedButtons.get(0).getText() + " y a: " + selectedButtons.get(1).getText()));

                            dialog.showAndWait();
                        }

                    }else{

                        selectedButtons.get(0).setGraphic(null);
                        selectedButtons.get(1).setGraphic(null);
                        selectedButtons.clear();


                    }


                });


            } else {

                tecnico.setOnMouseClicked(e -> {

                    selected = tecnico;
                    dialog.hide();

                });

            }

            for (LocalDate aDate : DatePicker.dates) {
                if (tecnicos.get(i).getDiaLibre().equals(aDate.getDayOfWeek().name())) {

                    tecnico.setDisable(true);
                }
            }

            String tecnicoName[] = tecnicos.get(i).getNombre().split("\\.");

            for (int e = 0; e < SohoApp.list.size(); e++) {

                for (LocalDate aDate : DatePicker.dates) {

                    if (!tecnico.isDisable() && tecnicoName[0].equals(SohoApp.list.get(e).getTecnico())
                            && SohoApp.list.get(e).getDate().toLocalDate().equals(aDate)) {

                        if (SohoApp.list.get(e).getDate().getHour() == hour) {

                            tecnico.setDisable(true);
                            System.out.println(SohoApp.list.get(e).getTecnico() + "DATE MATCHES");

                        } else if (SohoApp.list.get(e).getTiempoDeServ() == 2) {

                            if (SohoApp.list.get(e).getDate().getHour() == hour - 1) {

                                tecnico.setDisable(true);
                                System.out.println(SohoApp.list.get(e).getTecnico() + "DATE MATCHES - 1");


                            } else if (hour + 1 == SohoApp.list.get(e).getDate().getHour() && tiempoServ == 2) {

                                tecnico.setDisable(true);
                                System.out.println(SohoApp.list.get(e).getTecnico() + "DATE MATCHES + 1");

                            }

                        } else if (tiempoServ == 2) {

                            if (SohoApp.list.get(e).getDate().getHour() - 1 == hour) {

                                tecnico.setDisable(true);
                                System.out.println(SohoApp.list.get(e).getTecnico() + " -1 DATE MATCHES");


                            } else if (SohoApp.list.get(e).getDate().getHour() == 1 + hour) {

                                tecnico.setDisable(true);
                                System.out.println(SohoApp.list.get(e).getTecnico() + " +1 DATE MATCHES");


                            }


                        }

                    }
                }
            }

            bList.add(tecnico);
            //gridPane.add(tecnico,0,i);
            //cardPane.getItems().add(tecnico);
        }

        int index = 0;

        for (int i = 0; i < 3; i++) {

            for (int j = 0; j < bList.size() / 3; j++) {

                gridPane.add(bList.get(index), i, j);
                //if (index < 19) {
                index++;
                //}
            }

        }//JFXScrollPane.smoothScrolling(sp);
        gridPane.setVgap(10);
    }


}
