package com.GalaxyIO;

public class User {

    private int userID;
    private String userName;
    private String role;

    public User(int userID, String userName, String role) {

        this.userID = userID;
        this.userName = userName;
        this.role = role;

    }

    public int getUserID() {
        return userID;
    }

    public void setUserID(int userID) {
        this.userID = userID;
    }

    public String getUserName() {
        return userName;
    }

    public void setUserName(String userName) {
        this.userName = userName;
    }

    public String getRole() {
        return role;
    }

    public void setRole(String role) {
        this.role = role;
    }
}
