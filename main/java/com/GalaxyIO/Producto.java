package com.GalaxyIO;

public class Producto {

    private String nombreProducto;
    private int tiempoProducto;

    public Producto(String nombreProducto, int tiempoProducto) {
        this.nombreProducto = nombreProducto;
        this.tiempoProducto = tiempoProducto;
    }

    public String getNombreProducto() {
        return nombreProducto;
    }

    public void setNombreProducto(String nombreProducto) {
        this.nombreProducto = nombreProducto;
    }

    public int getTiempoProducto() {
        return tiempoProducto;
    }

    public void setTiempoProducto(int tiempoProducto) {
        this.tiempoProducto = tiempoProducto;
    }
}
