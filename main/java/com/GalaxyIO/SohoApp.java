package com.GalaxyIO;
//4114916

import com.GalaxyIO.views.AdminView;
import com.GalaxyIO.views.LoggedInView;
import com.GalaxyIO.views.PrimaryView;
import com.GalaxyIO.views.SecondaryView;
import com.gluonhq.charm.down.Platform;
import com.gluonhq.charm.glisten.application.MobileApplication;
import com.gluonhq.charm.glisten.license.License;
import com.gluonhq.charm.glisten.mvc.SplashView;
import com.gluonhq.charm.glisten.visual.Swatch;
import javafx.scene.Scene;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;
import javafx.scene.paint.Color;
import javafx.stage.Stage;
import javafx.util.Duration;

import java.util.ArrayList;

@License(key = "ba4a2305-92e4-4abe-8d34-736505b699b6")

public class SohoApp extends MobileApplication {

    public static final String PRIMARY_VIEW = HOME_VIEW;
    public static final String SECONDARY_VIEW = "Secondary View";
    public static Connectivity connectivity;
    public static ArrayList<Costumer> list = new ArrayList<>();
    public static ArrayList<Tecnico> tecnicoList = new ArrayList<>();
    public static ArrayList<Cliente> clienteList = new ArrayList<>();
    public static ArrayList<Producto> productoList = new ArrayList<>();
    public static ArrayList<Costumer> currentList = new ArrayList<>();

    @Override
    public void init() {

        addViewFactory(MobileApplication.SPLASH_VIEW, () -> new SplashView(HOME_VIEW,
                new ImageView(new Image("com/GalaxyIO/views/SOHO.png", 150, 150, true, true)),
                Duration.seconds(2)));

        addViewFactory(PRIMARY_VIEW, () -> new PrimaryView(PRIMARY_VIEW).getView());
        addViewFactory(SECONDARY_VIEW, () -> new SecondaryView(SECONDARY_VIEW).getView());
        addViewFactory("Another View", () -> new LoggedInView("Another View").LoggedInView());
        addViewFactory("Admin View", () -> new AdminView("Admin View").getView());

    }

    @Override
    public void postInit(Scene scene) {

        /*((Stage)scene.getWindow()).setOnCloseRequest(event -> {
            try {
                connectivity.conn.close();
            } catch (SQLException e) {
                e.printStackTrace();
            }
        });*/

        if (Platform.isDesktop()) {

            ((Stage) scene.getWindow()).setMaximized(true);
            //MobileApplication.getInstance().getView().autosize();

        } else if (Platform.isAndroid()) {

            getStatusBar().colorProperty().setValue(Color.BLACK);

        } else {

            getStatusBar().colorProperty().setValue(Color.WHITE);

        }

        Swatch.GREY.assignTo(scene);

        scene.getStylesheets().add(SohoApp.class.getResource("style.css").toExternalForm());
        //((Stage) scene.getWindow()).getIcons().add(new Image(SohoApp.class.getResourceAsStream("/icon.png")));


    }
}


